package com.samsoft.em.model



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Purchase)
class PurchaseTests {

    void testSomething() {
       def purchase = new Purchase(description : 'Test Purchase');
       purchase.addToItems(new Item(description:'test item',cost:500))
       purchase.addToItems(new Item(description:'test item',cost:600))
       purchase.addToItems(new Item(description:'test item',cost:700))
       purchase.addToItems(new Item(description:'test item',cost:300))
       assertNotNull purchase
       assertNotNull purchase.items
       int total = purchase.calculateTotalCost();
       assertTrue (2100==total)
    }
}
