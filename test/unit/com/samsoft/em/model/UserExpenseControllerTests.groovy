package com.samsoft.em.model



import org.junit.*
import grails.test.mixin.*

@TestFor(UserExpenseController)
@Mock(UserExpense)
class UserExpenseControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/userExpense/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.userExpenseInstanceList.size() == 0
        assert model.userExpenseInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.userExpenseInstance != null
    }

    void testSave() {
        controller.save()

        assert model.userExpenseInstance != null
        assert view == '/userExpense/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/userExpense/show/1'
        assert controller.flash.message != null
        assert UserExpense.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/userExpense/list'

        populateValidParams(params)
        def userExpense = new UserExpense(params)

        assert userExpense.save() != null

        params.id = userExpense.id

        def model = controller.show()

        assert model.userExpenseInstance == userExpense
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/userExpense/list'

        populateValidParams(params)
        def userExpense = new UserExpense(params)

        assert userExpense.save() != null

        params.id = userExpense.id

        def model = controller.edit()

        assert model.userExpenseInstance == userExpense
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/userExpense/list'

        response.reset()

        populateValidParams(params)
        def userExpense = new UserExpense(params)

        assert userExpense.save() != null

        // test invalid parameters in update
        params.id = userExpense.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/userExpense/edit"
        assert model.userExpenseInstance != null

        userExpense.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/userExpense/show/$userExpense.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        userExpense.clearErrors()

        populateValidParams(params)
        params.id = userExpense.id
        params.version = -1
        controller.update()

        assert view == "/userExpense/edit"
        assert model.userExpenseInstance != null
        assert model.userExpenseInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/userExpense/list'

        response.reset()

        populateValidParams(params)
        def userExpense = new UserExpense(params)

        assert userExpense.save() != null
        assert UserExpense.count() == 1

        params.id = userExpense.id

        controller.delete()

        assert UserExpense.count() == 0
        assert UserExpense.get(userExpense.id) == null
        assert response.redirectedUrl == '/userExpense/list'
    }
}
