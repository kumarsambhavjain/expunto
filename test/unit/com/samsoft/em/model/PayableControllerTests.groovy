package com.samsoft.em.model



import org.junit.*

import com.samsoft.em.controller.PayableController

import grails.test.mixin.*

@TestFor(PayableController)
@Mock(Payable)
class PayableControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/payable/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.payableInstanceList.size() == 0
        assert model.payableInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.payableInstance != null
    }

    void testSave() {
        controller.save()

        assert model.payableInstance != null
        assert view == '/payable/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/payable/show/1'
        assert controller.flash.message != null
        assert Payable.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/payable/list'


        populateValidParams(params)
        def payable = new Payable(params)

        assert payable.save() != null

        params.id = payable.id

        def model = controller.show()

        assert model.payableInstance == payable
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/payable/list'


        populateValidParams(params)
        def payable = new Payable(params)

        assert payable.save() != null

        params.id = payable.id

        def model = controller.edit()

        assert model.payableInstance == payable
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/payable/list'

        response.reset()


        populateValidParams(params)
        def payable = new Payable(params)

        assert payable.save() != null

        // test invalid parameters in update
        params.id = payable.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/payable/edit"
        assert model.payableInstance != null

        payable.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/payable/show/$payable.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        payable.clearErrors()

        populateValidParams(params)
        params.id = payable.id
        params.version = -1
        controller.update()

        assert view == "/payable/edit"
        assert model.payableInstance != null
        assert model.payableInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/payable/list'

        response.reset()

        populateValidParams(params)
        def payable = new Payable(params)

        assert payable.save() != null
        assert Payable.count() == 1

        params.id = payable.id

        controller.delete()

        assert Payable.count() == 0
        assert Payable.get(payable.id) == null
        assert response.redirectedUrl == '/payable/list'
    }
}
