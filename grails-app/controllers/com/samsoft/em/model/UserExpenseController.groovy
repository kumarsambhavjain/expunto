package com.samsoft.em.model

import grails.converters.JSON
import grails.plugins.springsecurity.Secured
import grails.plugins.springsecurity.SpringSecurityService

import org.springframework.dao.DataIntegrityViolationException

import com.samsoft.em.service.UserExpenseService

@Secured(['IS_AUTHENTICATED_REMEMBERED'])	
class UserExpenseController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	SpringSecurityService springSecurityService
	UserExpenseService userExpenseService

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        //params.max = Math.min(max ?: 10, 100)
        //[userExpenseInstanceList: UserExpense.list(params), userExpenseInstanceTotal: UserExpense.count()]
    }
	
	def search(){
		def fromDate = params.date('fromDate','dd-mm-yyyy')
		def toDate = params.date('toDate','dd-mm-yyyy')
		def d = params.fromDate
		
		if(fromDate == null || toDate == null || fromDate>toDate){
			// invalid inputs
			render '{"error":"Invalid Dates"}' as JSON
		}else{
			int f = params.frequency as int
			def exp = userExpenseService.getUserExpenses(fromDate,toDate,f)
			render(template:"userExpenseTemplate",model:[expenses: exp,expenseJSON: exp as JSON,freq:f,fromDate:params.fromDate,toDate:params.toDate])    
		}		
	}

    def create() {
        [userExpenseInstance: new UserExpense(params)]
    }

    def save() {
        def userExpenseInstance = new UserExpense(params)
		userExpenseInstance.user = User.read(springSecurityService.principal.id)
		if(userExpenseInstance.dateOfExpense==null){
			userExpenseInstance.dateOfExpense = new Date()
		}
        if (!userExpenseInstance.save(flush: true)) {
            render(view: "create", model: [userExpenseInstance: userExpenseInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'userExpense.label', default: 'UserExpense'), userExpenseInstance.id])
        redirect(action: "list")
    }

   

   /* def update(Long id, Long version) {
        def userExpenseInstance = UserExpense.get(id)
        if (!userExpenseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'userExpense.label', default: 'UserExpense'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (userExpenseInstance.version > version) {
                userExpenseInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'userExpense.label', default: 'UserExpense')] as Object[],
                          "Another user has updated this UserExpense while you were editing")
                render(view: "edit", model: [userExpenseInstance: userExpenseInstance])
                return
            }
        }

        userExpenseInstance.properties = params

        if (!userExpenseInstance.save(flush: true)) {
            render(view: "edit", model: [userExpenseInstance: userExpenseInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'userExpense.label', default: 'UserExpense'), userExpenseInstance.id])
        redirect(action: "show", id: userExpenseInstance.id)
    }*/

    def delete(Long id) {
        def userExpenseInstance = UserExpense.get(id)
        if (!userExpenseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'userExpense.label', default: 'UserExpense'), id])
            redirect(action: "list")
            return
        }

        try {
            userExpenseInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'userExpense.label', default: 'UserExpense'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'userExpense.label', default: 'UserExpense'), id])
            redirect(action: "show", id: id)
        }
    }
}
