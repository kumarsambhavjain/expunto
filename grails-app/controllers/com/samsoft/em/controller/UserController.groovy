package com.samsoft.em.controller

import grails.plugins.springsecurity.Secured

import org.springframework.dao.DataIntegrityViolationException

import com.samsoft.em.model.Membership
import com.samsoft.em.model.User
import com.samsoft.em.model.UserGroup



@Secured(['IS_AUTHENTICATED_REMEMBERED'])
class UserController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	static scope = "session"
	
	/**
	* Dependency injection for the springSecurityService.
	*/
	def springSecurityService
	def userService
	
	def loggedInUser = null;

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		//TODO: Handle list
		/*
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [userInstanceList: User.list(params), userInstanceTotal: User.count()]
        */
		redirect(controller:"home")
    }

    def create() {
        [userInstance: new User(params)]
    }

    def save() {
        def userInstance = new User(params)
        if (!userInstance.save(flush: true)) {
            render(view: "create", model: [userInstance: userInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
        redirect(action: "show", id: userInstance.id)
    }

    def show() {
		if(params.id){
			if(!loggedInUser)
			 loggedInUser = User.get(params.id)
		}
        if (!loggedInUser) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        [userInstance: loggedInUser]
    }

    def edit() {
        def userInstance = User.get(params.id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        [userInstance: userInstance]
    }

    def update() {
        def userInstance = User.get(params.id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (userInstance.version > version) {
                userInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'user.label', default: 'User')] as Object[],
                          "Another user has updated this User while you were editing")
                render(view: "edit", model: [userInstance: userInstance])
                return
            }
        }

        userInstance.properties = params

        if (!userInstance.save(flush: true)) {
            render(view: "edit", model: [userInstance: userInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
        redirect(action: "show", id: userInstance.id)
    }

    def delete() {
        def userInstance = User.get(params.id)
        if (!userInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        try {
            userInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
	
	/**
	 * This will let an exiting user to join an existing group. The user should pass groupKey request param.
	 * @return
	 */
	def joinGroup(){
		
		if(!params.groupKey){
			render(view:"joinGroup")
			return;
		}
		
		def group = UserGroup.findWhere(groupKey: params.groupKey )
		if(group){
			def user  = User.get(springSecurityService.principal.id)
			def m = Membership.link(user, group)
			if(!m){
			}else{
				flash.message = message(code:"user.added.to.group",default:"User added to group");
				redirect(view:"show",model:user)
			}
		}else{
			flash.errorMessage = message(code: 'user.incorrect.group.key',default:'Incorrect Group key' )
		}
	}
	
	def profile(){
		def user = springSecurityService.currentUser
		render(view:"profile",model:[user:user])
	}
	
	def changePassword(){
		String currentPassword = params.current
		String newPassword 	   = params.newPass
		String retyped 	   =     params.newPassRepeat
		if(currentPassword && newPassword && retyped && retyped.equals(newPassword)&&!newPassword.equals(currentPassword)){
			boolean changed = userService.changePassowrd(currentPassword, newPassword);
			if(changed){
				redirect(controller:"logout")
				return;
			}else{
				def user = springSecurityService.currentUser
				flash.errorMessage = message(code:"unable.to.change.password",default:"Sorry, unable to changed password.")
				render(view:"profile",model:[user:user])
			}
		}else{
			// input problem
			flash.errorMessage = message(code:"insufficient.input",default:"Incorrect inputs")
			def user = springSecurityService.currentUser
			render(view:"profile",model:[user:user])
		}
	}
	
}
