package com.samsoft.em.controller

import grails.converters.JSON
import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_REMEMBERED'])
class HomeController {

	def springSecurityService
	def payableService
	static scope = "session"

	def index() {

		String sortBy = params.sort?:"dateCreated"
		String order = params.order?:"desc";
		def receiveList = null
		def oweList = null
		if(sortBy == 'from'){
			receiveList = payableService.getUserRecievable('from',order);
			oweList = payableService.getUserPayableTos('to',order)
		}else if(sortBy == 'to'){
			receiveList = payableService.getUserRecievable('from',order);
			oweList = payableService.getUserPayableTos('to',order)
		}else{
			receiveList = payableService.getUserRecievable(sortBy,order);
			oweList = payableService.getUserPayableTos(sortBy,order)
		}

		def receiveTotal = 0
		Map<String,Integer[]> barMap = new HashMap<String,Integer[]>();
		Integer[] x=null,temp=null;
		Integer i = null
		receiveList?.each{ 
			x = barMap.get(it.from.fullName);
			if(x==null){
				temp = [0,0] as Integer[]
				temp[0] = it.amount
				barMap.put(it.from.fullName, temp)
			}else{
				x[0]+=it.amount
			}
			receiveTotal+=it.amount 
		}

		def oweTotal = 0
		oweList?.each{
			x = barMap.get(it.to.fullName);
			if(x==null){
				temp = [0,0] as Integer[]
				temp[1] = it.amount
				barMap.put(it.to.fullName, temp)
			}else{
				x[1]+=it.amount
			}
			oweTotal+=it.amount 
		}

		def netRec = receiveTotal - oweTotal
		boolean hasData = barMap.size()>0
		render (view:'home',model:["receiveList":receiveList,"oweList":oweList,"netRec":netRec,"receiveTotal":receiveTotal,"oweTotal":oweTotal,"barMap":barMap as JSON,"hasData":hasData])
	}

	def refreshRecievable(){
		def receiveList = payableService.getUserRecievable(params.sort,params.order);
	}
}
