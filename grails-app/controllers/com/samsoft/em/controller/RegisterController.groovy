package com.samsoft.em.controller


import net.tanesha.recaptcha.ReCaptchaImpl
import net.tanesha.recaptcha.ReCaptchaResponse

import com.samsoft.em.model.User
import com.samsoft.em.service.AccountService
import com.samsoft.em.service.MailUtilityService

class RegisterController {


	static scope = "request"

	def simpleCaptchaService
	MailUtilityService mailUtilityService
	AccountService accountService



	def index() {
		def user = new User();
		log.info('registering new user')
		render (view:"register",model:user)
	}

	def createNewUser(){
		/**
		 * Recaptcha validation.
		 */
		String remoteAddr = request.getRemoteAddr();
		ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
		reCaptcha.setPrivateKey("6Lf-ydgSAAAAANQMxE8ET-FS1VPgXuBC8_CdDwBx");
		String challenge = request.getParameter("recaptcha_challenge_field")
		String uresponse = request.getParameter("recaptcha_response_field")
		ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, uresponse)

		if (!reCaptchaResponse.isValid()) {
			// invalid captcha entered. send back.
			flash.invalidCaptcha = "invalid.captcha"
			redirect(controller:"login",action:"index")
			return
		}

		if(!params.password.equals(params.re_password)){
			flash.errorMessage = "password.not.same"
			redirect(controller:"login",action:"index")
			return
		}

		def userInstance = new User(params)
		if(!userInstance.validate()){
			// constraint error occurred. send back with Spring Errors.
			flash.userInstanceWithErrors = userInstance
			redirect(controller:"login",action:"index")
			return
		}

		accountService.createNewAccount(userInstance)
		flash.message = message(code:"user.account.created",default:"Account created sucessfully. Verfication mail sent to ${userInstance.username}")
		redirect(controller:"login",action:"index")
	}

	def verifyAccount(){
		String key = params.w960;
		String email = params.t810;

		if(key==null||email==null){
			flash.errorMessage = message(code:"unable.to.verify.account",default:"Unable to verify account")
			redirect(controller:"login",action:"index")
			return
		}
		if(accountService.verifyAccount(key,email)){
			flash.successMessage = message(code:"account.verfied",default:"Account verfied. Please login to continue.")
			redirect(controller:"login",action:"index")
			return
		}else{
			flash.errorMessage = message(code:"unable.to.verify.account",default:"Unable to verify account")
			redirect(controller:"login",action:"index")
			return
		}
	}
	
	def resetPassword(){
		String username = params.username
		if(username){
			/**
			 * Recaptcha validation.
			 */
			
			
			String remoteAddr = request.getRemoteAddr();
			ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
			reCaptcha.setPrivateKey("6Lf-ydgSAAAAANQMxE8ET-FS1VPgXuBC8_CdDwBx");
	
			String challenge = request.getParameter("recaptcha_challenge_field")
			String uresponse = request.getParameter("recaptcha_response_field")
			ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, uresponse)
	
			if (!reCaptchaResponse.isValid()) {
				// invalid captcha entered.
				flash.errorMessage ="Invalid captcha entered. Please retry."
				redirect(controller:"register",action:"resetPassword")
				return
			}
			if(accountService.resetPassword(username)){
				flash.successMessage = "New password sent to registered email."
				redirect(controller:"login",action:"auth")
				return
			}else{
				flash.errorMessage ="User not found."
				redirect(controller:"register",action:"resetPassword")
				return
			}
		}else{
			render(view:"reset")
		}
	}
}
