package com.samsoft.em.controller

import grails.converters.JSON
import grails.plugins.springsecurity.Secured

import org.springframework.dao.DataIntegrityViolationException

import com.samsoft.em.model.Membership
import com.samsoft.em.model.Payable
import com.samsoft.em.model.User
import com.samsoft.em.model.UserExpense
import com.samsoft.em.model.UserGroup

@Secured(['IS_AUTHENTICATED_REMEMBERED'])
class PayableController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	static scope = "session"
	def springSecurityService
	def userGroupService
	def payableService
	def authenticatedUser
	def theUser = null;
	def userService
	def mailUtilityService

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		/*if(!theUser)
		 theUser = User.read(springSecurityService.principal.id)
		 params.max = Math.min(params.max ? params.int('max') : 10, 100)
		 [payableInstanceList: Payable.list(params), payableInstanceTotal: Payable.count()]*/
		//chain(actionName:"search",params)
		redirect(action:"search",params:["menuSelected", "6"])
	}

	def create() {
		def currentUser = springSecurityService.currentUser
		def groupSet = Membership.executeQuery("select m.group from Membership m where m.user=?",[currentUser])
		if(!groupSet){
			flash.errorMessage =  message(code:"create.group.error",default:"Form a  Group with atleast two members")
		}
		[payableInstance: new Payable(params),groups:groupSet,currentUser:currentUser]
	}

	/**
	 * This will return users enrolled in a group JSON response
	 * @return
	 */
	def getGroupUsers(){
		def members = Membership.executeQuery("select m.user.id as id,m.user.fname as fname,m.user.lname as lname from Membership m where m.group.id=?",[params.group as long])
		if(!theUser)
			theUser = User.read(springSecurityService.principal.id)
		def finalList =  []
		members.each{
			if(it[0]!=springSecurityService.principal.id){
				finalList+=it
			}
		}
		members = null
		render finalList as JSON
	}

	def save() {

		log.info("adding new payable");
		if(!theUser)
			theUser = User.read(springSecurityService.principal.id)
		if(params.from==params.to){
			flash.errorMessage = message(code:"invalid.entry",default:"Invalid Entry")
			render(view: "create")
			return
		}
		if((params.to as long)!= theUser.id){
			flash.errorMessage = message(code:"invalid.to.user",default:"Invalid 'To' user")
			render(view: "create")
			return
		}
		def payableInstance = new Payable(description:params.description,from:User.get(params.from as long),to:User.get(params.to as long),amount:(params.amount as int),group:UserGroup.get(params.group as long),createdBy:theUser)
		if (!payableInstance.save(flush: true)) {
			render(view: "create", model: [payableInstance: payableInstance])
			return
		}
		new UserExpense(description:params.description,user:payableInstance.from,amount:payableInstance.amount).save()

		flash.message = message(code: 'default.created.message', args: [
			message(code: 'payable.label', default: 'Payable'),
			payableInstance.description
		])
		//Will be send it seperate thread!!
		mailUtilityService.sendPayableAlert(payableInstance)
		redirect(action: "show", id: payableInstance.id)
	}

	def show() {
		// TODO : Modify method to restrict payable of groups only. Else User will be able to see any payable detail
		def payableInstance = payableService.getPayableByID(params.id as long)
		if (!payableInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'payable.label', default: 'Payable'),
				params.id
			])
			redirect(action: "list")
			return
		}

		[payableInstance: payableInstance]
	}

	def edit() {
		// TODO: Analyze if we really need this functionality. Payable should only be mark paid. Commenting as of now.
		redirect(action:"search",params:["menuSelected", "6"])
		/*def payableInstance = Payable.get(params.id)
		 if (!payableInstance) {
		 flash.message = message(code: 'default.not.found.message', args: [
		 message(code: 'payable.label', default: 'Payable'),
		 params.id
		 ])
		 redirect(action: "list")
		 return
		 }
		 [payableInstance: payableInstance]*/
	}

	def update() {
		redirect(action:"search",params:["menuSelected", "6"])
		//TODO:  Same case as edit. Need to come up with strategy on payable changes.
		/*def payableInstance = Payable.get(params.id)
		 if (!payableInstance) {
		 flash.message = message(code: 'default.not.found.message', args: [
		 message(code: 'payable.label', default: 'Payable'),
		 params.id
		 ])
		 redirect(action: "list")
		 return
		 }
		 if (params.version) {
		 def version = params.version.toLong()
		 if (payableInstance.version > version) {
		 payableInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
		 [
		 message(code: 'payable.label', default: 'Payable')]
		 as Object[],
		 "Another user has updated this Payable while you were editing")
		 render(view: "edit", model: [payableInstance: payableInstance])
		 return
		 }
		 }
		 payableInstance.properties = params
		 if (!payableInstance.save(flush: true)) {
		 render(view: "edit", model: [payableInstance: payableInstance])
		 return
		 }
		 flash.message = message(code: 'default.updated.message', args: [
		 message(code: 'payable.label', default: 'Payable'),
		 payableInstance.id
		 ])
		 redirect(action: "show", id: payableInstance.id)
		 */
	}

	def delete() {
		def payableInstance = Payable.get(params.id)
		if (!payableInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'payable.label', default: 'Payable'),
				params.id
			])
			redirect(action: "list")
			return
		}

		try {
			payableInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [
				message(code: 'payable.label', default: 'Payable'),
				params.id
			])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [
				message(code: 'payable.label', default: 'Payable'),
				params.id
			])
			redirect(action: "show", id: params.id)
		}
	}

	def search(){
		/*if(!theUser)
		 theUser = User.read(springSecurityService.principal.id)*/
		def groupSet = Membership.executeQuery("select m.group from Membership m where m.user.id=?",[
			springSecurityService.principal.id
		])
		render(view:"search",model:[groupSet:groupSet,menuSelected:params.menuSelected])
	}

	/**
	 * return members in groups as JSON resposne
	 * @return
	 */
	def findUserForGroup(){
		if(params.groupCode){
			def ids = [] as Set
			params.groupCode.each{
				ids += (it as long)
			}
			def members = userGroupService.getUserInGroups(ids)
			render members as JSON
		}else{
			response.setContentType("application/json");
			render '{"errorMessage":"Invalid group code"}';
		}
	}

	/**
	 * Will actually perform the search and return data as json.
	 * @return
	 */
	def performPayableSearch(){

		def query = ""
		def grpList = []
		params.groupCode.each{
			grpList+= (it as long)
		}
		def frmList = []
		params.fromUser.each{
			frmList+=(it as long)
		}
		def toList = []
		params.toUser.each{
			toList+= (it as long)
		}

		def result = null;
		if(params.paid.size()==2){
			// both paid and unpaid
			result=Payable.executeQuery("select p from Payable p   where p.group.id in (:grpList) and p.from.id in (:fromList) and p.to.id in(:tooList) order by p.from.id asc",[grpList:grpList,fromList:frmList,tooList:toList])
		}else{
			if(params.paid=='0'){
				// unpaid
				result=Payable.executeQuery("select p from Payable p  where p.group.id in (:grpList) and p.transaction is null and  p.from.id in (:fromList) and p.to.id in(:tooList)",[grpList:grpList,fromList:frmList,tooList:toList])
			}else{
				//paid
				result=Payable.executeQuery("select p from Payable p  where p.group.id in (:grpList) and p.transaction.id > 0 and p.from.id in (:fromList) and p.to.id in(:tooList)",[grpList:grpList,fromList:frmList,tooList:toList])
			}
		}

		int total =0;
		result.each{ p ->
			total+=p.amount;
		}
		render (template:"genericPayableTemplate",model:[result:result,total:total])
	}

	def getSummaryInit(){
		def groups = userGroupService.getUsersGroups(springSecurityService.principal.id)
		render(view:"payableSummary",model:[groups:groups,menuSelected:params.menuSelected])
	}


	def normalisedPayable(){
		if(params.group){
			try{
				def payables = payableService.getNormalizedPayables(params.group as long)
				render (template:"normalizedPayablesTemplate",model:[result:payables])
			}catch(Exception ex){
				render '{"errorMessage":"Invalid group code"}';
			}
		}else{
			//TODO: Handle invalid group here.
			render '{"errorMessage":"Invalid group code"}';
		}
	}

	def saveNormalizedPayables(){
		//TODO: Check for group id param
		if(params.group){  // expects group id
			def txn = payableService.saveNormalizedPayableForGroup(params.group as long,params.message)
			log.info("saved normalized payable for group" + params.group)
			render txn as JSON
		}else{
			//TODO: Handle invalid group here.
			return null
		}
	}

	/**
	 * This handled ajax call. Expects pid and one comment as params  
	 * @return
	 */
	def saveMultiplePayables(){
		def idlist =  []
		def paramids = params.pid
		try{
			if(paramids.getClass().equals(String.class)){
				// only one was passed.
				idlist+= (paramids as long)
			}else{
				// multiple were passed.
				paramids.each{
					idlist+=(it as long)
				}
			}
		}catch( ex){
			render new Expando("error":"Invalid Payable") as JSON
			return
		}

		if(idlist.size()>0){
			def ret = payableService.markMultiplePaid(idlist, params.message);
			if (ret){
				render ret as JSON
			}else{
				render new Expando("error":"error") as JSON
				return
			}
		}else{
			// no IDS were found.
		}

	}
	
	def sendReminderMail(){
		Payable p = payableService.getPayableByID(params.pid as long)
		def res = [:]
		if(p){
			mailUtilityService.sendPayableReminder(p)
			res.sent = true
			render  res as JSON
			return
		}else{
			res.sent = false
			render res as JSON
			return
		}
	}
}
