package com.samsoft.em.controller

import grails.plugins.springsecurity.Secured

import com.samsoft.em.model.Payable

@Secured(['IS_AUTHENTICATED_REMEMBERED'])
class NormalizeHistoryController {


	static scope="request"
	def payableService
	def authenticatedUser

	/**
	 * Displays all the payable linked to this normalization history.
	 * @return
	 */
	def show() {
		long nid = -12,pid=-11;
		if(params.nid!=null&&params.pid!=null){
			try{
				nid = params.nid as long
				pid = params.pid as long
				def nh = payableService.getCoNormalizedPayableFor(nid,pid)
				def payables = nh.payables.sort{it.from.id}
				def payable = Payable.read(pid)
				def resultList = []
				normalizedHistorylist:payables.each{
					if(it.id!=payable.id){
						resultList.add(it)
					}
				}
				render (view:"show",model:[normalizedHistorylist:resultList,payable:payable])
			}catch(NumberFormatException ex){
				render (view:"show",model:[normalizedHistorylist:null,payable:null])
			}
		}else{
			render (view:"show",model:[normalizedHistorylist:null,payable:null])
		}
	}
}
