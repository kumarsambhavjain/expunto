package com.samsoft.em.controller

import grails.converters.JSON
import grails.plugins.springsecurity.Secured

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.servlet.ModelAndView

import com.samsoft.em.model.Membership
import com.samsoft.em.model.User
import com.samsoft.em.model.UserGroup



@Secured(['IS_AUTHENTICATED_REMEMBERED'])
class UserGroupController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	static scope = "session"
	
	/**
	* Dependency injection for the springSecurityService.
	*/
	def springSecurityService
	
	/**
	 * This will hold list of all groups to which the currently logged in user belongs to.
	 */
	def groupList

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
		def user  = User.read(springSecurityService.principal.id)
		render(view:"list",model:[userGroupInstanceList:user.memberships.group])
    }

    def create() {
        [userGroupInstance: new UserGroup(params),menuSelected:params.menuSelected]
    }

    def save() {
		def user  = User.get(springSecurityService.principal.id)
        def userGroupInstance = new UserGroup(params)
		userGroupInstance.createdBy = user
		userGroupInstance.moderator = user
		userGroupInstance.groupKey = generateRandomString(user.id)		
        if (!userGroupInstance.save(flush: true)) { // error occurred.
            render(view: "create", model: [userGroupInstance: userGroupInstance])
            return
        }
		Membership.link(user, userGroupInstance)
		def ids = params.memberList
		if(ids){
			ids.each(){
				Membership.link(User.get(it), userGroupInstance)
			}
		}
		flash.message = message(code: 'default.created.message', args: [message(code: 'userGroup.label', default: 'UserGroup'), userGroupInstance.groupName])
        redirect(action: "show", id: userGroupInstance.id)
    }
	
	public static String generateRandomString(long id){
		long random = -4;
		StringBuilder sb = new StringBuilder(8);
		for(int i=0;i<8;i++){
			random = Math.random()*10000000 
			if(random%2==0){
				sb.append((char)(random%25+65))
			}else{
				sb.append(random%10)
			}
			
		}
		sb.append(""+id);
		return sb.toString()
	}

    def show() {
        def userGroupInstance = UserGroup.get(params.id)
        if (!userGroupInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'userGroup.label', default: 'UserGroup'), params.id])
            redirect(action: "list")
            return
        }

        [userGroupInstance: userGroupInstance]
    }

    def edit() {
        def userGroupInstance = UserGroup.get(params.id)
        if (!userGroupInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'userGroup.label', default: 'UserGroup'), params.id])
            redirect(action: "list")
            return
        }

        [userGroupInstance: userGroupInstance]
    }

    def update() {
        def userGroupInstance = UserGroup.get(params.id)
        if (!userGroupInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'userGroup.label', default: 'UserGroup'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (userGroupInstance.version > version) {
                userGroupInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'userGroup.label', default: 'UserGroup')] as Object[],
                          "Another user has updated this UserGroup while you were editing")
                render(view: "edit", model: [userGroupInstance: userGroupInstance])
                return
            }
        }

        userGroupInstance.properties = params

        if (!userGroupInstance.save(flush: true)) {
            render(view: "edit", model: [userGroupInstance: userGroupInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'userGroup.label', default: 'UserGroup'), userGroupInstance.id])
        redirect(action: "show", id: userGroupInstance.id)
    }

    def delete() {
        def userGroupInstance = UserGroup.get(params.id)
        if (!userGroupInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'userGroup.label', default: 'UserGroup'), params.id])
            redirect(action: "list")
            return
        }

        try {
            userGroupInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'userGroup.label', default: 'UserGroup'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'userGroup.label', default: 'UserGroup'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
	
	
	/**
	 * The function will get users from all groups to which the currently logged-in user belongs.
	 * @return
	 */
	def getCoUsers(){
		def user  = User.get(springSecurityService.principal.id)
		def groups = Membership.executeQuery("select m.group from Membership m where m.user=?",[User.read(springSecurityService.principal.id)]);
		def users = [] as Set
		groups.each {
			it.memberships.each{
				users += it.user
			}
		}
		users.remove(user)
		render users as JSON
	}
	
	def joinExistingGroup(){
		return new ModelAndView("joinExistingGroup");		
	}
	
	def addUserToGroup(){
		def key = params.groupkey;
		def group  = UserGroup.findWhere(groupKey:key);
		if(!group){
			flash.errorMessage = message(code:"group.not.found",default:"Group not found.");	
			return new ModelAndView("joinExistingGroup");		
		}
		def user  = User.get(springSecurityService.principal.id)
		if(!user){
			flash.errorMessage = message(code:"user.not.found",default:"User not found.");
			return
		}
		def m = Membership.link(user, group)
		if(!m){
			flash.errorMessage = message(code:"already.added",default:"Already added in group  ${group.groupName}");
			render(view:"joinExistingGroup",model:[userGroupInstance:group])
		}else{
			flash.message = message(code:"user.added.to.group",default:"User added to group ${group.groupName}")
			render(view:"show",model:[userGroupInstance:group])
		}
	}
}
