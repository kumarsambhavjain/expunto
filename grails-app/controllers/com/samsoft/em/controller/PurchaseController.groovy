package com.samsoft.em.controller

import grails.converters.JSON
import grails.plugins.springsecurity.Secured

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.servlet.ModelAndView

import com.samsoft.em.model.Beneficiary
import com.samsoft.em.model.Item
import com.samsoft.em.model.Membership
import com.samsoft.em.model.Purchase
import com.samsoft.em.model.User
import com.samsoft.em.model.UserExpense
import com.samsoft.em.model.UserGroup
@Secured(['IS_AUTHENTICATED_REMEMBERED'])
class PurchaseController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	def userGroupService
	def springSecurityService
	def purchaseService
	def theUser = null;
	def mailUtilityService
	
	static scope = "session"
	
	private Purchase purchase = null;

    def index() {
        redirect(action: "list", params: params)
    }

	
	def search(){
		def groupSet = Membership.executeQuery("select m.group from Membership m where m.user.id=?",[
			springSecurityService.principal.id
		])
		return new ModelAndView("search", [ groupSet : groupSet ])
	}
	
    def list() {
        //params.max = Math.min(params.max ? params.int('max') : 10, 100)
        //[purchaseInstanceList: Purchase.list(params), purchaseInstanceTotal: Purchase.count()]
		redirect(action: "search")
    }

    def create() {
		def usersGroups = userGroupService.getUsersGroups(springSecurityService.principal.id);
		purchase = null;
		if(params.cancel){
			flash.message= null
			flash.errorMessage = message(code:"action.cancelled",default:"Action Cancelled")
		}
        [purchaseInstance: new Purchase(params),groups:usersGroups]
    }
	
	def cancel(){
		purchase = null
		redirect(action:"create",params:[cancel:"cancel"])
	}

    def savePurchase() {
		
		if(purchase){
			StringBuilder sb = new StringBuilder();
			int totalCost =0;
			int indivCost = 0;
			purchase.items.each {
				totalCost += it.cost
				sb.append(it.description+', ')
			}
			purchase.description=sb.substring(0, sb.length()-2);
			purchase.totalCost = totalCost
			if(!theUser)
				theUser = User.read(springSecurityService.principal.id)
			purchase.createdBy = theUser
			purchase.dateOfPurchase = new Date()
			purchase.group = UserGroup.read(params.group as long)
			
			def benf = purchase.beneficiaries
			purchase.beneficiaries = null;
			indivCost = totalCost/benf.size()
			benf.each{
				new UserExpense(description:purchase.description,amount:indivCost,user:it.user,dateOfExpense:purchase.dateOfPurchase).save(failOnError:true)
				purchase.addToBeneficiaries(it)
			}
			
			def items = purchase.items
			purchase.items = null;
			items.each{
				purchase.addToItems(it)
			}
			def payables = purchase.payables
			purchase.payables = null
			payables.each{
				it.description = purchase.description
				it.createdBy = theUser
				it.group = purchase.group
				purchase.addToPayables(it)
			}
			//TODO: Validate against constraint before saving.
			if (!purchase.save(flush: true,failOnError:true)) {
				log.error("Unable to save the purchase to database.")
				return				
			}
			log.info('Purchase saved successfully.')
			flash.message = message(code: 'purchase.created.successfully',default:"Purchase created successfully")
			def pid = purchase.id
			mailUtilityService.sendPurchaseAlert(purchase)
			purchase = null;   // removed from the session so that it wont be saved again.
			redirect(action: "show", id: pid)
			//return
			// celebrate. purchase was saved successfully.
		}else{
			// session has expired.
		}
		
		/*
        def purchaseInstance = new Purchase(params)
        if (!purchaseInstance.save(flush: true)) {
            render(view: "create", model: [purchaseInstance: purchaseInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'purchase.label', default: 'Purchase'), purchaseInstance.id])
        redirect(action: "show", id: purchaseInstance.id)
        */
    }

    def show() {
        /*def purchaseInstance = Purchase.get(params.id)
        if (!purchaseInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchase.label', default: 'Purchase'), params.id])
            redirect(action: "list")
            return
        }*/
		if(params.id){
			def purchaseInstance = purchaseService.getPurchaseDetailsByID(params.id as Long)
			if(purchaseInstance)
				[purchaseInstance: purchaseInstance,indvCost:(purchaseInstance.totalCost/purchaseInstance.beneficiaries.size()) as int]
			else
				[purchaseInstance: null]
		}else{
			[purchaseInstance: null]
		}
    }

    def edit() {
        def purchaseInstance = Purchase.get(params.id)
        if (!purchaseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchase.label', default: 'Purchase'), params.id])
            redirect(action: "list")
            return
        }

        [purchaseInstance: purchaseInstance]
    }

    def update() {
        def purchaseInstance = Purchase.get(params.id)
        if (!purchaseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchase.label', default: 'Purchase'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (purchaseInstance.version > version) {
                purchaseInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'purchase.label', default: 'Purchase')] as Object[],
                          "Another user has updated this Purchase while you were editing")
                render(view: "edit", model: [purchaseInstance: purchaseInstance])
                return
            }
        }

        purchaseInstance.properties = params

        if (!purchaseInstance.save(flush: true)) {
            render(view: "edit", model: [purchaseInstance: purchaseInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'purchase.label', default: 'Purchase'), purchaseInstance.id])
        redirect(action: "show", id: purchaseInstance.id)
    }

    def delete() {
        def purchaseInstance = Purchase.get(params.id)
        if (!purchaseInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchase.label', default: 'Purchase'), params.id])
            redirect(action: "list")
            return
        }

        try {
            purchaseInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'purchase.label', default: 'Purchase'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'purchase.label', default: 'Purchase'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
	
	/**
	 * Returns group member id and fname as json reponse.
	 * @return Members as JSON
	 */
	def getGroupMembers(){
		def members = null;
		if(params.group){
			members = userGroupService.getGroupMembers(params.group as long);
		}
		render members as JSON;
	}
	
	def renderGroupBeneficiaries(){
		def members = null;
		if(params.group){
			members = userGroupService.getGroupMembers(params.group as long);
			render(template:"benfTable",model:[members:members])
		}
	}
	
	
	/**
	 * This will construct purchase instance from params and use purchase service to compute the payables for this purchase.
	 * @return  will render the table showing the payables.
	 */
	def computePayables(){
		log.info('Will compute purchase now.')
		
		def itemDesc = params.description
		def itemCost = params.cost
		
		def itemList = new ArrayList<Item>(itemCost.size());
		int cartCost = 0;
		if(itemCost.getClass().equals(String.class) ){
			itemList.add(new Item(cost:itemCost as int,description:itemDesc))
			cartCost = itemCost as int
		}else{
			for(int i=0;i<itemCost.size();i++){
				itemList.add(new Item(cost:itemCost[i] as int,description:itemDesc[i]))
				cartCost+= itemCost[i] as int
			}
		}
		
		
		def bids = params.bid
		def amountContributed = params.amountContributed
		int totalContr = 0;
		
		if(bids.getClass().equals(String.class) ){
			// there should be more than 1 beneficiaries.
			render '{"errorMessage":"'+message(code:"atleast.two.benf",default:"There should be atleast two beneficiaries")+'"}' 
			return;
		}
		params.amountContributed.each{
			totalContr += it as int
		}
		
		if(totalContr!=cartCost){
			// this is although handled by javascript.
			render '{"errorMessage":"'+message(code:"cost.mismatch",default:"Total cost must be equal to total contribution")+'"}' 
			return;
		}
		
		def beneficiaries = new ArrayList<Beneficiary>(bids.size());
		for(int i=0;i<bids.size();i++){
			beneficiaries.add(new Beneficiary(amountContributed:params.amountContributed[i] as int,user:User.get(bids[i] as long)))
		}
		
		// purchase is an attribute of this session scoped controller.
		purchase = new Purchase();
		purchase.items = itemList
		purchase.beneficiaries = beneficiaries
		def payables = purchaseService.evaluatePayables(purchase)
		log.info("rendering payable result.");
		int total =0;
		payables?.each{
			total+=it.amount;
		}
		render(template:"computedPayablesTemplate",model:[result:payables,total:total,individualCost:cartCost/beneficiaries.size()])
		return
		
	}
}
