import com.samsoft.em.model.Membership
import com.samsoft.em.model.Payable
import com.samsoft.em.model.User
import com.samsoft.em.model.UserExpense
import com.samsoft.em.model.UserGroup
import com.samsoft.em.model.sec.SecRole
import com.samsoft.em.model.sec.SecUserSecRole


class BootStrap {

		def init = { servletContext ->


				def userRole = SecRole.findByAuthority('ROLE_USER') ?: new SecRole(authority: 'ROLE_USER').save(failOnError: true)
				def adminRole = SecRole.findByAuthority('ROLE_ADMIN') ?: new SecRole(authority: 'ROLE_ADMIN').save(failOnError: true)
				def adminModerator = SecRole.findByAuthority('ROLE_MODERATOR') ?: new SecRole(authority: 'ROLE_MODERATOR').save(failOnError: true)

				environments  {
						production  {
								// perform production specific task here.   prod or production??
								servletContext.setAttribute("env", "prod")
						}
						development {
								servletContext.setAttribute("env", "dev")
								def adminUser1 = User.findByUsername('kumar.sambhav.jain@gmail.com') ?: new User(
												username: 'kumar.sambhav.jain@gmail.com',
												password: 'ksambhav', fname:'User',lname:'A',mobile:'9167450141',
												enabled: true).save(failOnError: true)

								def adminUser2 = User.findByUsername('karanmiglani.jiit@gmail.com') ?: new User(
												username: 'karanmiglani.jiit@gmail.com',
												password: 'ksambhav', fname:'User',lname:'B',mobile:'9167450143',
												enabled: true).save(failOnError: true)

								def adminUser3 = User.findByUsername('rohit@gmail.com') ?: new User(
												username: 'rohit@gmail.com',
												password: 'ksambhav', fname:'User',lname:'C',mobile:'9167454444',
												enabled: true).save(failOnError: true)

								def adminUser4 = User.findByUsername('shan@gmail.com') ?: new User(
												username: 'shan@gmail.com',
												password: 'ksambhav', fname:'User',lname:'D',mobile:'9167450152',
												enabled: true).save(failOnError: true)

												def adminUser5 = User.findByUsername('shan1@gmail.com') ?: new User(
													username: 'shan1@gmail.com',
													password: 'ksambhav', fname:'User',lname:'E',mobile:'9167450134',
													enabled: true).save(failOnError: true)
	
													
													def adminUser6 = User.findByUsername('shan2@gmail.com') ?: new User(
														username: 'shan2@gmail.com',
														password: 'ksambhav', fname:'User',lname:'F',mobile:'8167450152',
														enabled: true).save(failOnError: true)
		
								if (!adminUser1.authorities.contains(adminRole)) {
										SecUserSecRole.create adminUser1, adminRole
										SecUserSecRole.create adminUser1, adminModerator
										SecUserSecRole.create adminUser2, userRole
										SecUserSecRole.create adminUser4, userRole
										SecUserSecRole.create adminUser5, userRole
										SecUserSecRole.create adminUser6, userRole
										SecUserSecRole.create adminUser3, userRole
								}


								def dummyGroup =  UserGroup.findByGroupKey("ABCDEF")?:new UserGroup(groupName: "Flatmates",createdBy:adminUser1,moderator: adminUser1,groupKey: "ABCDEF").save(flush:true,failOnError:true)
								Membership.link(adminUser1, dummyGroup)
								Membership.link(adminUser2, dummyGroup)
								Membership.link(adminUser3, dummyGroup)
								Membership.link(adminUser4, dummyGroup)
								Membership.link(adminUser5, dummyGroup)
								Membership.link(adminUser6, dummyGroup)


								def p1 = new Payable(description: "Loan",amount: 5000,createdBy: adminUser1,  from: adminUser1, to: adminUser3, group: dummyGroup  ).save(flush:true,failOnError:true)
								def p2 = new Payable(description: "Beer",amount: 2000,createdBy: adminUser3,  from: adminUser2, to: adminUser4, group: dummyGroup  ).save(flush:true,failOnError:true)
								def p3 = new Payable(description: "Movie",amount: 3000,createdBy: adminUser4,  from: adminUser2, to: adminUser1,   group: dummyGroup).save(flush:true,failOnError:true)
								def p4 = new Payable(description: "Flight",amount: 3420,createdBy: adminUser1,  from: adminUser3, to: adminUser1,   group: dummyGroup).save(flush:true,failOnError:true)
								def p5 = new Payable(description: "Train",amount: 500,createdBy: adminUser1,  from: adminUser3, to: adminUser2,  group: dummyGroup ).save(flush:true,failOnError:true)
								def p6 = new Payable(description: "Misc",amount: 800,createdBy: adminUser2,  from: adminUser4, to: adminUser3,  group: dummyGroup ).save(flush:true,failOnError:true)
								def p7 = new Payable(description: "Fruits",amount: 90,createdBy: adminUser2,  from: adminUser4, to: adminUser2, group: dummyGroup  ).save(flush:true,failOnError:true)
								def p8 = new Payable(description: "Grocery",amount: 6454,createdBy: adminUser2,  from: adminUser2, to: adminUser1,  group: dummyGroup ).save(flush:true,failOnError:true)
								int t = 0;
								Date d = new Date()
								d.set(year:2012)
								d.set(month:11)
								d.set(date:31)
								UserExpense ex = null
								for(int i=0;i<365*2;i++){
									t = (Math.random()*100+100)
									ex=new UserExpense(description:"Test",amount:t,dateOfExpense:d.minus(i))
									ex.user = adminUser1
									ex.save(flush:true,failOnError:true)
								}
								
								
						}
				}
		}
		def destroy = {
		}
}