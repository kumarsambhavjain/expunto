<%@ page import="com.samsoft.em.model.User" %>



<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="user.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="username" required="" value="${userInstance?.username}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="user.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="password" name="password" required="" value="${userInstance?.password}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'fname', 'error')} required">
	<label for="fname">
		<g:message code="user.first.name.label" default="First Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="fname" required="" value="${userInstance?.fname}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'lname', 'error')} required">
	<label for="lname">
		<g:message code="user.last.name.label" default="Last Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="lname" required="" value="${userInstance?.lname}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'mobile', 'error')} ">
	<label for="mobile">
		<g:message code="user.mobile.label" default="Mobile" />
		
	</label>
	<g:textField name="mobile" value="${userInstance?.mobile}"/>
</div>
