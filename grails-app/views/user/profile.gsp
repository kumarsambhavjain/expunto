<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
td.field{
	font-weight: bold;
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>
	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.errorMessage }">
					<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			${flash.errorMessage}
					</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class="span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp" value="${params.menuSelected}" />
			</div>
			<div class=" span8" id="mainContent">
				<g:form class="form-horizontal" action="changePassword"  method="post">
					<fieldset>
						<legend>
							<g:message code="your.profile" default="Your Profile" />
						</legend>
					</fieldset>
					<div class="span5 offset1">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td class="field"><g:message code="name" default="Name"/></td>
								<td class="value">${user.fname} ${user.lname}</td>
							</tr>
							<tr>
								<td class="field"><g:message code="mobile" default="Mobile"/></td>
								<td class="value">${user.mobile?:"-" }</td>
							</tr>
							<tr>
								<td class="field"><g:message code="joined.on" default="Joined On"/></td>
								<td class="value"><g:formatDate date="${user.dateCreated }"/>  </td>
							</tr>
							<tr>
								<td class="field"><g:message code="Password" default="Password"/></td>
								<td class="value">
										<a href="#"><g:message code="change.password" default="Change Password. Will require re-login"/></a>
										<br><input  pattern=".{6,40}"  name="current" required="required" type="password" placeholder="Current Password" style="margin-top: 5px;"/>
										<br><input  pattern=".{6,40}" name="newPass" required="required" type="password" placeholder="New Password" style="margin-top: 5px;"/>
										<br><input  pattern=".{6,40}" name="newPassRepeat" required="required" type="password" placeholder="Re-type New Password" style="margin-top: 5px;"/>
										<input type="submit" class="btn btn-danger" value="Change">
								</td>
							</tr>
						</tbody>
					</table>
					</div>
				</g:form>
			</div>
		</div>
	</div>
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
</body>
</html>