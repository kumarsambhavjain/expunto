<%@ page contentType="text/html;charset=ISO-8859-1"%>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'user.join.group', default: 'Join a Group')}" />
<title><g:message code="user.join.group" default="Join a group" /></title>
</head>
<body>
	<div class="body"></div>
	<g:form method="post">
		<fieldset class="form">
			<label for="groupKey"> <g:message code="user.group.key"
					default="Group Key" /> <span class="required-indicator">*</span>
			</label>
			<g:textField name="groupKey" required="required" />
		</fieldset>
		<fieldset class="buttons">
			<g:actionSubmit class="save" action="joinGroup" value="${message(code: 'user.gruop.join', default: 'Join')}" />
		</fieldset>
	</g:form>
</body>
</html>