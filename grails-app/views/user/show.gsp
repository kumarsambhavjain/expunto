<%@ page import="com.samsoft.em.model.User"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>
	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.errorMessage }">
					<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			${flash.errorMessage}
					</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class="span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp" value="${params.menuSelected}" />
			</div>
			<div class=" span8" id="mainContent">
				<g:form class="form-horizontal">
					<fieldset>
						<legend>
							<g:message code="User.details" default="User Details" />
						</legend>

						<g:if test="${userInstance?.username}">
							<div class="control-group">
								<label class="control-label attribute"> <g:message
										code="user.email" default="Email" />
								</label>
								<div class="controls">
									<span class="input-xlarge uneditable-input"> ${userInstance.username }
									</span>
								</div>
							</div>
						</g:if>

						<g:if test="${userInstance?.fname}">
							<div class="control-group">
								<label class="control-label attribute"> <g:message
										code="user.name" default="Name" />
								</label>
								<div class="controls">
									<span class="input-xlarge uneditable-input"> ${userInstance.fname }
									</span>
								</div>
							</div>
						</g:if>

						<g:if test="${userInstance?.mobile}">
							<div class="control-group">
								<label class="control-label attribute"> <g:message
										code="user.mobile" default="Mobile" />
								</label>
								<div class="controls">
									<span class="input-xlarge uneditable-input"> ${userInstance.mobile }
									</span>
								</div>
							</div>
						</g:if>

						<g:if test="${userInstance?.dateCreated}">
							<div class="control-group">
								<label class="control-label attribute"> <g:message
										code="user.date.created" default="Date Created" />
								</label>
								<div class="controls">
									<span class="input-xlarge uneditable-input"> <g:formatDate
											date="${userInstance.dateCreated}" />

									</span>
								</div>
							</div>
						</g:if>

						<!-- 
						<div class="control-group">
							<label class="control-label attribute"> <g:message
									code="user.bank.details" default="Bank Details" />
							</label>

							<g:if test="${userInstance?.bank }">
								<span class="input-xlarge uneditable-input"> ${userInstance.fname }
								</span>
							</g:if>
							<g:else>
								<div class="controls" style="padding: 4px">
									<g:link url="#">
										<g:message code="add.bank.details" default="Add Bank Details" />
									</g:link>
								</div>
							</g:else>
						</div> -->

					</fieldset>
				</g:form>
			</div>
		</div>
	</div>
	<!-- /container -->

	
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-modal.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-scrollspy.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-tooltip.js"></g:javascript>
	<g:javascript src="bootstrap-popover.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>
	<g:javascript src="bootstrap-carousel.js"></g:javascript>
	<g:javascript src="bootstrap-typeahead.js"></g:javascript>

	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>

</body>
</html>
