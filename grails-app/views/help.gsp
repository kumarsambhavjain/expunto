<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />
<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">

<%--<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="${resource(file:'apple-touch-icon-72-precomposed.png',dir:'images') }">
<link rel="apple-touch-icon-precomposed"
	href="${resource(file:'apple-touch-icon-57-precomposed.png',dir:'images') }">
--%>
</head>

<body>

	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.message }">
				<div class="alert alert-info">
					<a class="close" data-dismiss="alert">×</a>
					${flash.message}
				</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class="span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"  params="[menuSelected:params.menuSelected]"/>
			</div>
			<div class="span8" id="mainContent">

			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-popover.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="utility.js"></g:javascript>

	<script type="text/javascript">
		
	</script>

</body>
</html>
