<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
		
			<div class="row-fluid">
				<div class="span12">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> 
					<span class="icon-bar"></span> <span class="icon-bar"></span>
					</a> <a class="brand" href="#">Expunto</a>
					<div class="nav-collapse">
						<ul class="nav">
							<li class="active"><g:link controller="home"><g:message code="home" default="Home" /></g:link></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"><g:message code="report"
										default="Report" /> <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><g:link target="_blank"
											url="https://bitbucket.org/kumarsambhavjain/expunto/issues/new">
											<g:message code="issues" default="Bug / Suggestions" />
										</g:link></li>
								</ul>
							</li>
						</ul>
						<div class="btn-group pull-right">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="icon-user"></i><sec:username /> <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><g:link controller="user" action="profile"><g:message code="profile" default="Profile" /></g:link></li>
								<li class="divider"></li><li><g:link controller="logout"><g:message code="logout" default="Logout" /></g:link></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
