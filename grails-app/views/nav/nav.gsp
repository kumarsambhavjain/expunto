<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
<ul class="nav nav-list">
	<g:hiddenField name="menuSelected" id="menuSelected" value="${params.menuSelected}"/>

	<li class="nav-header"><g:message code="nav.group.management" default="Group Management"/></li>
	<g:link></g:link>
	<li id="1"><g:link class="navi"  controller="userGroup"   action="create"><i class="icon-plus"></i><g:message code="create.new.group" default="Create New Group"/></g:link></li>
	<li id="2"><g:link class="navi" controller="userGroup" action="joinExistingGroup"><i class="icon-share"></i><g:message code="join.existing.group" default="Join Existing Group"/></g:link></li>
	<li id="3"><g:link class="navi" controller="userGroup" action="list"><i class="icon-list"></i><g:message code="groups" default="Groups"/></g:link></li>
	
	<li class="nav-header"><g:message code="nav.purchase" default="Purchase"/></li>
	
	<li id="4"><g:link class="navi" controller="purchase" action="create"><i class="icon-plus"></i> <g:message code="add.purchase" default="Add Purchase"/></g:link></li>
	<!-- <li><g:link class="navi" id="1" controller="purchase" action="search"><i class="icon-search"></i> <g:message code="search.purchase" default="Search Purchase"/></g:link></li> -->
	
	<li class="nav-header"><g:message code="nav.payable" default="Payable"/>  </li>
	<li id="5"><g:link class="navi" controller="home" action="index"><i class="icon-list-alt"></i> <g:message code="your.summary" default="Your Summary"/></g:link></li>
	<li id="6"><g:link class="navi" controller="payable" action="create"><i class="icon-plus"></i> <g:message code="add.payable" default="Add Payable"/></g:link></li>
	<li id="7"><g:link class="navi" controller="payable" action="search"><i class="icon-search"></i> <g:message code="search.payable" default="Search Payable"/></g:link></li>
	<li id="8"><g:link class="navi" controller="payable" action="getSummaryInit"><i class="icon-list"></i> <g:message code="normalized" default="Normalized"/></g:link></li>
	
	<li class="nav-header"><g:message code="your.expenses" default="Your Expenses"/>  </li>
	<li id="9"><g:link class="navi" controller="userExpense" action="create"><i class="icon-plus alt"></i> <g:message code="add.expense" default="Add Expense"/></g:link></li>
	<li id="10"><g:link class="navi" controller="userExpense" action="list"><i class="icon-list alt"></i> <g:message code="track.expense" default="Track Expense"/></g:link></li>
	
	<li class="divider"></li>
	<%--<li><g:link class="navi" id="8" controller="payable" action="getSummaryInit"><i class="icon-flag"></i> <g:message code="help" default="Help"/></g:link></li>--%>	
</ul>


<g:javascript src="jquery.js"></g:javascript>
<script type="text/javascript">
	
	$(document).ready(function(){
		/*
		var count=0;
		var link = '';
		$('a.navi').each(function(){
			$(this).attr('id',count);
			link = $(this).attr('href');
			link+=('?menuSelected='+count);
			$(this).attr('href',link);
			count++;
		});

		*/
		var selectedMenu = $('#menuSelected').attr('value');
		if(selectedMenu){
			$('a').removeClass('active');
			$('li').removeClass('active');
			$('i').removeClass('icon-white');
			var selectedAnchor = $('#'+selectedMenu);
			$(selectedAnchor).addClass('active');
			//console.log($('>i'));  //TODO make the icon white once selected.
		}
		
	});
</script>
