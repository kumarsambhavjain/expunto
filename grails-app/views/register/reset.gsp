<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="google-site-verification" content="nrCASrrxkej3Y-oW4ZBrGxKlkP9q7KU8Ki_mHXavvVI" />
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Kumar Sambhav Jain">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
<script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'white'
 };
 </script>
</head>

<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></a>  
				<g:link class="brand" controller="home">Expunto</g:link>
				<ul class="nav">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"><g:message code="report"
										default="Report" /> <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><g:link target="_blank"
											url="https://bitbucket.org/kumarsambhavjain/expunto/issues/new">
											<g:message code="issues" default="Bug / Suggestions" />
										</g:link></li>
								</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="span5">
				<div id="id_errorMessage">
					<g:if test="${flash.errorMessage }">
						<div class="alert alert-error">
				  			<a class="close" data-dismiss="alert">×</a>
				  			${flash.errorMessage}
						</div>
					</g:if>
				</div>
				<g:form class="form-horizontal" method="post" action="resetPassword" controller="register" name="id_resetPassword" useToken="true">
					<%
			          ReCaptcha c = ReCaptchaFactory.newReCaptcha("6Lf-ydgSAAAAAGO0fzfvupxiWQyEB06URyuSEmMP", "6Lf-ydgSAAAAANQMxE8ET-FS1VPgXuBC8_CdDwBx", false);
			        %>
					<fieldset>
						<legend>
							<g:message code="reset.password"
								default="Reset Password" />
						</legend>
						<div class="control-group">
							<label class="control-label" for="username"><g:message
									code="email" default="Email" />*</label>
							<div class="controls">
								<input type="email" class="input-xlarge focused" id="username"
									name="username" required="required" autofocus="autofocus" value="${username }"
									autocomplete="on" placeholder="Your Registered Email Address" title="Email will be sent here"> 
									<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="captcha"><g:message
									code="user.captcha" default="Captcha" />*</label>
									
							<div class="controls">
								<%= out.print(c.createRecaptchaHtml(null, null))%>
							</div>
						</div>	
					</fieldset>
					<div class="form-actions">
							<input type="submit" class="btn btn-primary" value="Reset" id="submitButton"/>
							<input type="button" class="btn btn-danger" value="Cancel" onclick="window.location = '../login';"/>
					</div>
				</g:form>
			</div>
		</div>
	</div>
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	</body>
</html>