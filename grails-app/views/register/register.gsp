<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse"
					data-target=".nav-collapse"> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="icon-bar"></span>
				</a> <g:link class="brand" controller="home">Expunto</g:link>
				<ul class="nav">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"><g:message code="report"
										default="Report" /> <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><g:link target="_blank"
											url="https://bitbucket.org/kumarsambhavjain/expunto/issues/new">
											<g:message code="issues" default="Bug / Suggestions" />
										</g:link></li>
								</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container">
		<div id="id_message">
			<g:if test="${flash.errorMessage }">
					<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			${flash.errorMessage}
					</div>
			</g:if>
			<g:if test="${flash.message }">
					<div class="alert alert-success">
			  			<a type="button" class="close" data-dismiss="alert">×</a>
			  			${flash.message}
					</div>
			</g:if>
			<g:if test="${flash.invalidCaptcha }">
					<div class="alert alert-error">
			  			<a type="button" class="close" data-dismiss="alert">×</a>
			  			<g:message code="${flash.invalidCaptcha }" default="Invalid Captcha. Please retry."/>
					</div>
			</g:if>
			<g:if test="${flash.userInstanceWithErrors}">
				<g:eachError bean="${flash.userInstanceWithErrors}">
					<g:if test="${flash.userInstanceWithErrors.errors.hasFieldErrors("username") }">
						<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			<g:message code="username.exists" default="A user is already registered with email '${it.rejectedValue}'"/>
						</div>						
					</g:if>
					<g:else>
					<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			<g:message error="${it }"/>
					</div>
					</g:else>
				</g:eachError>
			</g:if>
		</div>
		<div class="row">
			<div class="span6">
				<div id="id_errorMessage"></div>
				<g:form class="form-horizontal" method="post" action="createNewUser" name="id_createNewUser" useToken="true">
					<%
			          ReCaptcha c = ReCaptchaFactory.newReCaptcha("6Lf-ydgSAAAAAGO0fzfvupxiWQyEB06URyuSEmMP", "6Lf-ydgSAAAAANQMxE8ET-FS1VPgXuBC8_CdDwBx", false);
			        %>
					<fieldset>
						<legend>
							<g:message code="user.create.new.account"
								default="Create New Account" />
						</legend>

						<div class="control-group">
							<label class="control-label" for="username"><g:message
									code="email" default="Email" />*</label>
							<div class="controls">
								<input type="text" class="input-xlarge focused" id="username"
									name="username" required="required" autofocus="autofocus"
									autocomplete="on" placeholder="Your Email Address"> 
									<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="fname"><g:message
									code="user.password" default="Password" />*</label>
							<div class="controls">
								<input type="password" class="input-xlarge" id="password"
									name="password" required="required" placeholder="Minimum 6 characters">
									<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="re_password"><g:message
									code="user.retype.password" default="Retype Password" />*</label>
							<div class="controls">
								<input type="password" class="input-xlarge" id="re_password"
									name="re_password" required="required" placeholder="Same as above"/> <span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="fname"><g:message
									code="user.first.name" default="First Name" />*</label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="fname" name="fname"
									required="required" size="40" placeholder="Minimum 3 characters"/>
									<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="lname"><g:message
									code="user.last.name" default="Last Name" /></label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="lname" name="lname"
									size="40">
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="mobile"><g:message
									code="user.mobile" default="Mobile" /></label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="mobile"
									name="mobile" size="10" /><span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="captcha"><g:message
									code="user.captcha" default="Captcha" />*</label>
									
							<div class="controls">
								<%= out.print(c.createRecaptchaHtml(null, null))%>
							</div>
						</div>						
						<div class="form-actions">
							<input type="button" class="btn btn-primary" value="Submit" id="submitButton"/>
							<input type="button" class="btn btn-danger" value="Cancel" />
						</div>
					</fieldset>
				</g:form>
			</div>
			
			<%--<div class="span7 offset0">
				<legend>
							<g:message code="simple.and.easy"
								default="Simple and Easy" />
				</legend>
				<div id="myCarousel" class="carousel">
				  <!-- Carousel items -->
				  <div class="carousel-inner">
				    
				    <div class="active item">	<img alt="" src="/expunto/static/images/addPur2.png"></div>
				    <div class="item">	<img alt="" src="/expunto/static/images/summ.png"></div>
				    <div class="item">	<img alt="" src="/expunto/static/images/norm.png"></div>
				    <div class="item"><img alt="" src="/expunto/static/images/addPur.png"></div>
				  </div>
				  <!-- Carousel nav -->
				  <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				  <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
				</div>
			</div>
			
			
		--%></div>
		
		
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-carousel.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>

	<script type="text/javascript">
		$(document).ready(function() {
			var emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$/;
			
			$('input').change(function(){
				$(this).parent().parent().removeClass("error");
				$(this).parent().find('span.help-inline').html('');
			});
			
			$('#username').change(function(){
				if(!emailRegex.test($(this).attr('value'))){
					$(this).parent().parent().addClass("error");
					$(this).parent().find('span.help-inline').html('Invalid email');
				}
			});

			$('#re_password').change(function(){
				if($('#password').attr('value').length>0){
					if(!($('#password').attr('value')==$('#re_password').attr('value'))){
						$('#re_password').parent().parent().addClass("error");
						$('#re_password').parent().find('span.help-inline').html('Do not match');
					}
				}
			});
			
			$('#submitButton').click(function(){
				if(validateForm()){
					$('#id_createNewUser').submit();
				}
			});

			function markErrorOnField(fieldElement,message,reloadCaptcha){
				$(fieldElement).parent().parent().addClass("error");
				$(fieldElement).parent().find('span.help-inline').html(message);
				if(reloadCaptcha){
					$('#id_captcha_image').attr('src','simpleCaptcha/captcha?dummy='+new Date());
					$('#id_captcha').attr('value',null);
				}
			}

			function validateForm(){
				var isValid = true;
				$('input[required=required]').each(function(){
					if($(this).attr('value').length==0){
						$(this).parent().parent().addClass("error");
						$(this).parent().find('span.help-inline').html('Required field');
						isValid = false;
						return false;
					}
				});

				if(isValid){
					if($('#password').attr('value').length<6){
						$('#password').parent().parent().addClass("error");
						$('#password').parent().find('span.help-inline').html('Minimum 6 characters');
						isValid = false;
						return false;
					}
				}

				if(isValid){
					if(!$('#password').attr('value')==$('#re_password').attr('value')){
						$('#re_password').parent().parent().addClass("error");
						$('#re_password').parent().find('span.help-inline').html('Do not match');
						isValid = false;
						return false;
					}
				}			
				if(isValid){
					$("input[type=button]").addClass("disabled");
				}
				return isValid;
			}			
			$('.btn-danger').click(function(){
				window.location.href = "../login";
			});
		});
	</script>
</body>
</html>
