<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>
	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.message }">
				<div class="alert alert-info">
					<a class="close" data-dismiss="alert">×</a>
					${flash.message}
				</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class="span2">
				<g:include view="nav/nav.gsp" 	params="[menuSelected:5]" />
			</div>
			<div class="span8" id="mainContent">
				<!-- The Home page should show summary of currently logged in users payables.-->
				<legend id="id_legend">
					<g:message code="your.summary" default="Your Summary" />
				</legend>
				<div class="alert alert-info">
					<g:if test="${netRec>0 }">
						<strong><g:message code="net.receivable"
								default="Net Receivable" /></strong>:<g:formatNumber number="${netRec }"
							type="number" />
					</g:if>
					<g:else>
						<strong><g:message code="net.payable"
								default="Net Payable" /></strong>:<g:formatNumber number="${netRec*-1 }"
							type="number" />
					</g:else>
				</div>
				<div class="tabbable">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#firstTab" data-toggle="tab"><g:message code="summary" default="Summary" /></a></li>
						<li class="active"><a href="#p1" data-toggle="tab"><g:message code="receivables" default="Receivables" /></a></li>
						<li><a href="#p2" data-toggle="tab"><g:message code="payables" default="Payables" /></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="firstTab" style="width: 900px; height: 500px;">
							
						</div>
						<div class="tab-pane" id="p1">
							<table class="table table-striped" id="id_receivables">
								<thead>
									<tr class="headerRow">
										<th width="2%"><input type="checkbox" id="id_checkAll" /></th>
										<g:sortableColumn title="Date" titleKey="date" property="dateCreated"/>
										<g:sortableColumn title="Group Name" titleKey="group.name" property="group.groupName"/>
										<g:sortableColumn title="From" titleKey="from" property="from"/>
										<g:sortableColumn title="Amount" titleKey="amount" property="amount"/>
										<th style="width: 25%"><g:message code="description" default="Description" /></th>
										<g:sortableColumn title="Created By" titleKey="created.by" property="createdBy"/>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<g:if test="${receiveList?.size==0 }">
										<tr>
											<td colspan="6"><g:message code="no.receivables"
													default="No Receivables" /></td>
										</tr>
									</g:if>
									<g:else>
										<g:each var="r" in="${receiveList}" status="c">
											<tr>
												<td><input type="checkbox" name="pid" value="${r.id }" /></td>
												<td><g:formatDate date="${r.dateCreated }" /></td>
												<td>${r.group.groupName }</td>
												<td>
													${r.from.getFullName() }
												</td>
												<td><g:formatNumber number="${r.amount }" type="number" />
												</td>
												<td>
													${r.description}
												</td>
												<td>
													${r.createdBy.getFullName() }
												</td>
												<td style="margin-left: 20px">														
													<div class="dropdown" id="menu${c}">										    						    
														<a class="dropdown-toggle" data-toggle="dropdown" href="#menu${c}"> <g:message code="details" default="Details"/>  <b class="caret"></b></a>
													    <ul class="dropdown-menu">
													    	<g:if test="${r.purchase!=null }">
													      		<li><g:link controller="purchase" action="show" id="${r.purchase?.id}"><g:message code="show.purchase" default="Show Purchase"/></g:link></li>
													      	</g:if>
													      	<g:else test="${r.purchase==null}">
													      		<li><g:link controller="payable" action="show" id="${r.id}"><g:message code="show.payable" default="Show Payable"/></g:link></li>
													      	</g:else>
													      	<g:if test="${r.normalizeHistory }">
													      		<li><g:link controller="normalizeHistory" action="show" params="[pid:r.id,nid:r.normalizeHistory]"><g:message code="normalization.history" default="Normalization History"/></g:link></li>
													      	</g:if>
													      	<li><a href="#" onclick="sendMail(${r.id})"><g:message code="send.reminder" default="Send Reminder"/></a></li>
													    </ul>
												    </div>
												</td>
											</tr>
										</g:each>
									</g:else>
									<g:if test="${receiveList?.size>0 }">
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td style="font-weight: 900; font-size: large;"><g:message
													code="total" default="Total" /></td>
											<td style="font-weight: 900; font-size: large;"><g:formatNumber
													number="${receiveTotal }" type="number" />
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</g:if>
								</tbody>
							</table>
							<g:if test="${receiveList?.size>0 }">
								 <div class="row pull-right">
										<input class="btn btn-danger disabled" type="button" value="Mark Recieved" id="id_markPaid" data-toggle="modal" href="#id_confirmation" />
								</div>
							</g:if>
						</div>
						<div class="tab-pane fade" id="p2">
							<table class="table table-striped">
								<thead>
									<tr class="headerRow">
										<g:sortableColumn title="Date" titleKey="date" property="dateCreated"/>
										<g:sortableColumn title="Group Name" titleKey="group.name" property="group.groupName"/>
										<g:sortableColumn title="To" titleKey="to" property="to"/>
										<g:sortableColumn title="Amount" titleKey="amount" property="amount"/>
										<th style="width: 25%"><g:message code="description" default="Description" /></th>
										<g:sortableColumn title="Created By" titleKey="created.by" property="createdBy"/>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<g:if test="${oweList?.size==0 }">
										<tr>
											<td colspan="5"><g:message code="no.payables"
													default="No Payables" /></td>
										</tr>
									</g:if>
									<g:else>
										<g:each var="r" in="${oweList}" status="c">
											<tr>
												<td><g:formatDate date="${r.dateCreated }" /></td>
												<td>${r.group.groupName }</td>
												<td>
													${r.to.getFullName() }
												</td>
												<td><g:formatNumber number="${r.amount }" type="number" />
												</td>
												<td>
													${r.description}
												</td>
												<td>
													${r.createdBy.getFullName() }
												</td>
												<td>
													<div class="dropdown" id="pmenu${c}">										    						    
														<a class="dropdown-toggle" data-toggle="dropdown" href="#pmenu${c}"> <g:message code="details" default="Details"/>  <b class="caret"></b></a>
													    <ul class="dropdown-menu">
													    	<g:if test="${r.purchase!=null }">
													      		<li><g:link controller="purchase" action="show" id="${r.purchase?.id}"><g:message code="show.purchase" default="Show Purchase"/></g:link></li>
													      	</g:if>
													      	<g:else test="${r.purchase==null}">
													      		<li><g:link controller="payable" action="show" id="${r.id}"><g:message code="show.payable" default="Show Payable"/></g:link></li>
													      	</g:else>
													      	<g:if test="${r.normalizeHistory }">
													      		<li><g:link controller="normalizeHistory" action="show" params="[pid:r.id,nid:r.normalizeHistory]"><g:message code="normalization.history" default="Normalization History"/></g:link></li>
													      	</g:if>
													    </ul>
												    </div>
												</td>
											</tr>
										</g:each>
									</g:else>
									<g:if test="${oweList?.size>0 }">
										<tr>
											<td></td>
											<td></td>
											<td style="font-weight: 900; font-size: large;"><g:message
													code="total" default="Total" /></td>
											<td style="font-weight: 900; font-size: large;"><g:formatNumber
													number="${oweTotal }" type="number" />
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</g:if>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div> <!-- main content ends here -->
		</div>  <!-- Row fluid ends here. -->
		<div id="id_confirmation" class="modal hide fade">
					<div class="modal-header">
						<a class="close" data-dismiss="modal">×</a>
						<h2>
							<g:message code="confirmation" default="Confirmation" />
						</h2>
					</div>
					<div class="modal-body">
						<h4 id="id_infoMessage">These will be marked as paid:-</h4>
						<div id="id_ctable" style="margin-top: 15px">
							<table class="table table-striped">
								<thead>
									<tr class="headerRow">
										<th><g:message code="date" default="Date" /></th>
										<th><g:message code="from" default="From" /></th>
										<th><g:message code="amount" default="Amount" /></th>
										<th><g:message code="description" default="Description" /></th>
									</tr>
								</thead>
								<tbody id="id_ct_tbody">
								</tbody>
							</table>
						</div>
						<div class="controls" id="id_commentsInputArea">
							<label for="comment"> <g:message code="comments"
									default="Comments" />*
							</label>
							<textarea name="comment" style="width: 520px; height: 100px;"
								draggable="false" required="required" contenteditable="true"
								autofocus="autofocus" placeholder="Your comments..."></textarea>
						</div>
						<p id="id_cmessage"></p>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn" id="id_closeButton">Close</a> <a href="#"
							id="id_saveButton" class="btn btn-primary">Save</a>
					</div>
		</div> <!-- modal confirmation ends here -->
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-modal.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-tooltip.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>
	<g:javascript src="utility.js"></g:javascript>
	<g:javascript src="chart.js"></g:javascript>
	 <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript">
			var barMap = ${barMap};
			var hasData = ${hasData};
			if(hasData){
			google.load("visualization", "1", {packages:["corechart"]});
		      function drawChart() {
			    var dataArray = [['User', 'Receivables', 'Payables']];
			    for(var name in barMap){
				    dataArray.push([name,barMap[name][0],barMap[name][1]]);
				}
		        var data = google.visualization.arrayToDataTable(dataArray);

		        var options = {
		          title: 'Summary',height:'350',width:'800',animation:{
		              duration: 1000,
		              easing: 'out',
		            },
		          hAxis: {title: 'User', titleTextStyle: {color: 'red'},chartArea:{left:0,top:10}}
		        };

		        var chart = new google.visualization.ColumnChart(document.getElementById('firstTab'));
		        chart.draw(data, options);
		      }
		      google.setOnLoadCallback(drawChart);
			}else{
				$("#firstTab").html("<p>No chart available</p>");
			}
			// Send Mail function
			function sendMail(pid){
				$.ajax({
					type	:	"post",
					url		:	"../payable/sendReminderMail?pid="+pid,
					cache	:	false,
					success	:	function(result){
									if(result.sent){
										showMessage("Reminder Mail Sent","success");
									}else{
										showMessage("Unable to send Reminder Mail","error");
									}
								},
					error	:	function(error){
									showMessage("Unable to send Reminder Mail","error");
								}
				});
			}
	</script>
		
	<script type="text/javascript">
	$(document).ready(function() {
		$('.dropdown-toggle').dropdown();
		var cp = '<%=request.getContextPath()%>';
		$("ul.nav-tabs>li:first").addClass("active");
		$('input[name=pid]').change(function() {
				var one = false;
				var all = true;
				$('input[name=pid]').each(function() {
					if ($(this).attr('checked') == 'checked') {
						one = true;
					} else {
						all = false;
					}
				});
				if (all || one) {
					$('#id_markPaid').removeClass('disabled');
				} else {
					$('#id_markPaid').addClass('disabled');
				}
				if (all) {
					$('#id_checkAll').attr('checked','checked');
				} else {
					$('#id_checkAll').removeAttr('checked','checked');
				}
		});

		$('#id_checkAll').click(function() {
			var selected = $(this).prop("checked");
			$('#id_receivables>tbody>tr').each(function() {
				$("input[type=checkbox]",$(this)).prop("checked",selected);
			});
			if (selected) {
				$('#id_markPaid').removeClass(
						'disabled');
			} else {
				$('#id_markPaid').addClass(
						'disabled');
			}
		});

		$('#id_closeButton').click(function(event) {
			event.preventDefault();
			$('#id_confirmation').modal('hide');
			$('#id_markPaid').removeClass('disabled');
		});

		$('#id_saveButton').click(function(event) {
				event.preventDefault();
				var idiv = $('#id_commentsInputArea');
				var comment = $("textarea",$(idiv)).val().trim();
				if (comment == null|| comment.length == 0) {
					idiv.attr('class','control-group error');
					return;
				}
				// Perform Save operation here.
				$.ajax({
					type : "post",
					url : "../payable/saveMultiplePayables?message="+ comment,
					data : $('input[name=pid]:checked').serializeArray(),
					success : function(result) {
								$('div.modal').modal('hide');
								 window.location.reload(true);
								showMessage("Action performed successfully","success");
							  },
				  	error	:function(error){
						  		$('div.modal').modal('hide');
								showMessage("Internal Error","error");
					  		 }
				});
			});

		$('#id_markPaid').click(function(event) {
			if ($('#id_markPaid').hasClass('disabled')) {
				event.preventDefault();
				return;
			}
			var checked = $('input[name=pid]:checked');
			if (checked.length == 0) {
				event.preventDefault();
				return;
			}

		});

		$('div.modal').on('show',function(event) {
				$('#id_markPaid').addClass(
						'disabled');
				var checked = $('input[name=pid]:checked');
				if (checked.length == 0) {
					$('div.modal').modal('hide');
					event.preventDefault();
					return;
				} else {
					$('#id_ct_tbody').html('');
					$('input[name=pid]:checked').each(function() {
							var tr = $(this).parent().parent();
							$('#id_ct_tbody').append("<tr><td>"+ $('td:nth-child(2)',tr).html()+ "</td><td>"+ $('td:nth-child(3)',tr).html()+ "</td><td>"+ $('td:nth-child(4)',tr).html()+ "</td><td>"+ $('td:nth-child(5)',tr).html()+ "</td></tr>");
					});
				}
		});
	});
	</script>
		
</body>
</html>
