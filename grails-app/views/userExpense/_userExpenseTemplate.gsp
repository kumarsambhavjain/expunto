<div id="id_graph" style="width: 900px; height: 400px;">
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var exp = ${expenseJSON};
		var f = parseInt(${freq});
		var toDate ='${toDate}'.split('-');
		var fromDate = '${fromDate}'.split('-');
		toDate = new Date(toDate[2],toDate[1]-1,toDate[0]);
		fromDate = new Date(fromDate[2],fromDate[1]-1,fromDate[0]);
    	var options = null;
    	var data = new google.visualization.DataTable();
    	var chart = null;
		switch(f){
			case 0://weekly
				 options = {
			        title: 'Weekly Expense Report'
			      };
				break;
			case 1://monthly
				 options = {
			        title: 'Monthly Expense Report'
			      };
				 data.addColumn('date', 'Month - Year');
					data.addColumn('number', 'Expense');
					for (var i=0;i<exp.length;i++){
						data.addRow([ new Date(exp[i][0],exp[i][1]-1,1) ,exp[i][2]]);				
					}
				break;
			case 2://yearly
				 options = {
					        title: 'Yearly Expense Report'
					      };
				data.addColumn('string', 'Year');
				data.addColumn('number', 'Expense');
				for (var i=0;i<exp.length;i++){
					data.addRow([exp[i][0]+'',exp[i][1]]);				
				}
				break; 
		}
		chart = new google.visualization.LineChart(document.getElementById("id_graph"));
	      chart.draw(data, options);
	});
</script>
