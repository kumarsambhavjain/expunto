<%@ page import="com.samsoft.em.model.UserExpense" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>
	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.message }">
				<div class="alert alert-info">
					<a class="close" data-dismiss="alert">×</a>
					${flash.message}
				</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class="span2">
				<g:include view="nav/nav.gsp" 	params="[menuSelected:9]" />
			</div>
			<div class="span8" id="mainContent">
				<g:form class="form-horizontal" method="post" action="save">
					<legend id="id_legend">
						<g:message code="your.summary" default="Add Expense" />
					</legend>
					<div class="control-group">
							<label class="control-label" for="description"><g:message
									code="description" default="Description" />*</label>
							<div class="controls">
								<input type="text" required="required" placeholder="Expense description" maxlength="300" name="description"/>
							</div>
					</div>
					<div class="control-group">
							<label class="control-label" for="amount"><g:message
									code="amount" default="Amount" />*</label>
							<div class="controls">
								<input type="number" required="required" placeholder="Amount" maxlength="300" name="amount" min="1" max="50000"/>
							</div>
					</div>
					<div class="form-actions">
						<input type="submit" class="btn btn-primary" value="Add" id="submitButton" /> 
						<input type="button" class="btn btn-danger" value="Cancel" />
					</div>
				</g:form>			
			
			</div> <!-- main content ends here -->
		</div>  <!-- Row fluid ends here. -->
	
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-modal.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>
	<g:javascript src="utility.js"></g:javascript>
	<g:javascript src="chart.js"></g:javascript>
  </body>
</html>

