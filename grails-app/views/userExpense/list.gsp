
<%@ page import="com.samsoft.em.model.UserExpense" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'jquery-ui-1.9.2.custom.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">// google charts
	google.load("visualization", "1", {packages:["corechart"]});
</script>
</head>

<body>
	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.message }">
				<div class="alert alert-info">
					<a class="close" data-dismiss="alert">×</a>
					${flash.message}
				</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class="span2">
				<g:include view="nav/nav.gsp" 	params="[menuSelected:10]" />
			</div>
			<div class="span8" id="mainContent">
				<form id="id_form">
						<legend id="id_legend">
								<g:message code="your.expenses" default="Your Expenses"/>
						</legend>
						<div class="row">
							<div class="span3 offset1">
								<label class="search-label"><g:message code="from" default="From"/></label>
								<input type="text" maxlength="20" id="fromDate" name="fromDate"/>
							</div>
							<div class="span3">
								<label class="search-label"><g:message code="to" default="To"/></label>
								<input type="text" maxlength="20" id="toDate" name="toDate"/>
							</div>
								<div class="span3 offset1">
									<label class="search-label"><g:message code="frequency" default="Frequency"/></label>
									<select name='frequency' id="id_freq">
										<%--<option value="0"><g:message code="weekly" default="Weekly"/></option>
										--%><option value="1"><g:message code="monthly" default="Monthly"/></option>
										<option value="2"><g:message code="yearly" default="Yearly"/></option>
									</select>
								</div>
						</div>
						<div class="row">
							<div class="offset1">
								<input class="btn" type="button" value="Apply" id="id_applyButton"/>
							</div>
        	  			</div>
				</form>		
				<div class="row-fluid" id="id_result">
				</div>				
			</div> <!-- main content ends here -->
		</div>  <!-- Row fluid ends here. -->
	</div>
	<!-- /container -->
</body>
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-modal.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>
	<g:javascript src="utility.js"></g:javascript>
	<g:javascript src="chart.js"></g:javascript>
	<g:javascript src="jquery-ui-1.9.2.custom.js"></g:javascript>
	<script type="text/javascript">
		var fromDate = null;
		var toDate = null;
		$(document).ready(function(){
			$("#fromDate").datepicker({
	            changeMonth: true,
	            changeYear: true,
	            dateFormat:'d MM, y',
	            maxDate:'0',
	            defaultDate: -30
	        });
			$("#toDate").datepicker({
	            changeMonth: true,
	            changeYear: true,
	            dateFormat:'d MM, y',
	            maxDate:'0',
	            defaultDate: 0
	        });
	        $("#id_applyButton").click(function(){
				$("#id_result").html('');
				fromDate=  $.datepicker.formatDate('dd-mm-yy',  $("#fromDate").datepicker("getDate"));  
				toDate=  $.datepicker.formatDate('dd-mm-yy',  $("#toDate").datepicker("getDate"));
				if(fromDate==null||toDate==null){
					// handle error
				}else{
					$.ajax({
						url		:	"search?fromDate="+fromDate+"&toDate="+toDate+"&frequency="+$('#id_freq').val(),
						success	:	function(result){
										$("#id_result").html(result);
									},
						error	:	function(error){
										$("#id_result").html(error);
									}	
					});
				}
			});
		});
	</script>

</html>

