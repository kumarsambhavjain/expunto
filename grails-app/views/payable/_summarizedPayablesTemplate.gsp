<form>
	<legend id="id_legend">
		<g:message code="summarized.payables" default="Summarized Payables" />
	</legend>
	<table class="table table-striped" style="margin-top: 30px">
		<thead style="table-layout: fixed;">
			<tr class="headerRow">
				<th><g:message code="date" default="From" /></th>
				<th><g:message code="date" default="To" /></th>
				<th><g:message code="date" default="Amount" /></th>
			</tr>
		</thead>
		<tbody id="id_payableSearchResult">
			<g:if test="${result==null || result.size==0 }">
				<tr>
					<td colspan="7"><g:message code="no.results.found"
							default="No Results Found" /></td>
				</tr>
			</g:if>
			<g:else>
				<g:each in="${result}" var="p">
					<tr>
						<td><g:link controller="user" action="show"
								params="[id:p.from.id]">
								${p.from.fname}
							</g:link></td>
						<td><g:link controller="user" action="show"
								params="[id:p.to.id]">
								${p.to.fname}
							</g:link></td>
						<td><g:formatNumber number="${p.amount }" type="number" /></td>
					</tr>
				</g:each>
				<%--<tr>
					<td></td>
					<td style="font-weight: 900; font-size: large;"><g:message
							code="total" default="Total" /></td>
					<td style="font-weight: 900; font-size: large;"><g:formatNumber
							number="${total }" type="number" /></td>
					<td></td>
				</tr>
			--%></g:else>
		</tbody>
	</table>
</form>