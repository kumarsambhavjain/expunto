<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<script type="text/javascript" src="../js/utility.js"></script>
<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>
	<g:include view="/nav/topBar.gsp"/>
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.errorMessage }">
					<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			${flash.errorMessage}
					</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class=" span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"  params="[menuSelected:7]"/>
			</div>
			<div class="span8" id="mainContent">
					<form>
						<legend id="id_legend">
								<g:message code="search.payables" default="Search Payable"/>
						</legend>
							<div id="id_SearchArea">	
								<div class="row">
									<div class="span3 offset1">
										<label class="search-label"><g:message code="group" default="Group"/></label>
								  		<select name="groupCode" id="id_groupSelect" multiple="multiple" size="2">
							  				<g:each in="${groupSet}" var="group">
							  					<option value="${group.id}">${group.groupName }</option>
							  				</g:each>
								  		</select>
									</div>
									<div class="span3 offset1">
										<label class="search-label"><g:message code="status" default="Status"/></label>
								  		<select name="paid" multiple="multiple" size="2">
								  			<option value="0" selected="selected"><g:message code="unpaid" default="Unpaid"/></option>
								  			<option value="1"><g:message code="paid" default="Paid"/></option>
								  		</select>
									</div>
								</div>
								<div class="row">	
									<div class="span3 offset1">
										<label class="search-label"><g:message code="from" default="From"/></label>
								  		<select name="fromUser" id="id_fromUser" disabled="disabled" class="userSelect" multiple="multiple">
								  		</select>
									</div>
									<div class="span3 offset1">
										<label class="search-label"><g:message code="to" default="To"/></label>
								  		<select name="toUser" id="id_toUser" disabled="disabled" class="userSelect" multiple="multiple">
								  		</select>
									</div>
					     	   </div>
					     	    <div class="row">
					     	   		<div class="span3 offset1">
					     	   				<input class="btn disabled " type="button" value="Search" id="id_SearchButton"/>
					     	   		</div>
			     	  			 </div>
				     	   </div> <!-- End of search Area -->
			     	  
					</form>
				
				<div id="resultArea" class="toBeLoaded">
				</div>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-popover.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>
	<g:javascript src="utility.js"></g:javascript>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#id_groupSelect').focus();
			if($('#id_groupSelect > option').length==1){
				// there is only one group available.select it.
				//  not working
				//$('#id_groupSelect > option').attr('selected','selected');
				//$('select[name=paid]').focus();
			}

			
			$('#id_groupSelect').focusout(function(){
				$.ajax({
					type	:	"post",
					cache	:	true,
					url		:	"findUserForGroup",
					data	:	$('#id_groupSelect').serializeArray(),
					success	:	function(response){
									console.log(response);
									fillUserDropdown($('#id_fromUser'),response);
									fillUserDropdown($('#id_toUser'),response);
								},
					error	:	function(error){
									showMessage('Internal Error.Retry.','error');
								}
				});
			});


			$('select.userSelect').change(function(){
				var enable = true;
				$('select.userSelect').each(function(){
					if($(this).val()==-1){
						enable =false;
					}
				});	

				if(enable==true){
					$('#id_SearchButton').removeClass("disabled");
				}
			});

			$('#id_legend').click(function(){
				$("#id_SearchArea").slideToggle("fast");
			});


			$('#id_SearchButton').click(function(){
				if(!$(this).hasClass('disabled')){
					clearMessage();
					var from = $('#id_fromUser').val();
					var to = $('#id_toUser').val();
					if(from==to&&from!='-9'){
						showMessage("Choose 'All' or different users","error");
					}else{
						//performSearch
						$('#resultArea').html("");
						$.ajax({
							url		:		"performPayableSearch",
							type	:		"post",
							data	:		$('form').serializeArray(),
							success	:		function(result){
												$('#resultArea').html(result);
												$("#id_SearchArea").slideUp("fast");
											},
							error	:		function(error){
												//console.log(error);
												$('#resultArea').html("");
												showMessage('Internal Error.Retry.','error');
											}
						});
					}
				}
			});
			
			$('form').bind('keypress',function(e){
				if(!$('#id_SearchButton').hasClass('disabled')&&e.which==13){
					$('#id_SearchButton').click();
				}
			});
		});

		function fillUserDropdown(dd,data){
			$('option',dd).remove();
			dd.removeAttr('disabled');
			for(var i=0;i<data.length;i++){
				dd.append('<option value="'+data[i].uid+'">'+data[i].name+'</option>');
			}
		}
	</script>

</body>
</html>
