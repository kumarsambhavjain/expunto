<!-- Generic template for showing payables -->
<table class="table table-striped">
	<thead>
		<tr class="headerRow">
			<g:sortableColumn title="Date" titleKey="date" property="dateCreated" />
			<g:sortableColumn title="To" titleKey="to" property="to" />
			<g:sortableColumn title="Amount" titleKey="amount" property="amount" />
			<th><g:message code="status" default="Status"/> </th>
			<th><g:message code="description" default="Description" /></th>
			<g:sortableColumn title="Created By" titleKey="created.by"
				property="createdBy" />
			<th></th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${result?.size==0 }">
			<tr>
				<td colspan="5"><g:message code="no.payables"
						default="No Payables" /></td>
			</tr>
		</g:if>
		<g:else>
			<g:each var="r" in="${result}" status="c">
				<tr>
					<td><g:formatDate date="${r.dateCreated }" /></td>
					<td>
						${r.to.fname }
					</td>
					<td><g:formatNumber number="${r.amount }" type="number" /></td>
					<td>
						<g:if test="${r.transaction }">
							<g:if test="${r.normalizeHistory }">
								<a href="#" rel="tooltip" data-placement="top" data-original-title="On <g:formatDate date="${r.transaction.dateCreated}"/>">
									<g:message code="normalized.paid" default="Normalized & Mark Paid"/>
								</a>	
							</g:if>
							<g:else>							
								<a href="#" rel="tooltip" data-placement="top" data-original-title="On <g:formatDate date="${r.transaction.dateCreated}"/>">
									<g:message code="paid.on" default="Paid"/>
								</a>
							</g:else>
						</g:if>
						<g:else>
							<g:message code="unpaid" default="Unpaid"/>
						</g:else>
					</td>
					<td>
						${r.description}
					</td>
					<td>
						${r.createdBy.fname }
					</td>
					<td>
						<div class="dropdown" id="pmenu${c}">
							<a class="dropdown-toggle" data-toggle="dropdown"
								href="#pmenu${c}"> <g:message code="details"
									default="Details" /> <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<g:if test="${r.purchase!=null }">
									<li><g:link controller="purchase" action="show"
											id="${r.purchase?.id}">
											<g:message code="show.purchase" default="Show Purchase" />
										</g:link></li>
								</g:if>
								<g:else test="${r.purchase==null}">
									<li><g:link controller="payable" action="show"
											id="${r.id}">
											<g:message code="show.payable" default="Show Payable" />
										</g:link></li>
								</g:else>
								<g:if test="${r.normalizeHistory }">
									<li><g:link controller="normalizeHistory" action="show"
											params="[pid:r.id,nid:r.normalizeHistory]">
											<g:message code="normalization.history"
												default="Normalization History" />
										</g:link></li>
								</g:if>
							</ul>
						</div>
					</td>
				</tr>
			</g:each>
		</g:else>
	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){
	$('a[rel=tooltip]').tooltip();
});
</script>