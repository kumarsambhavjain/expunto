<%--
<%@ page import="com.samsoft.em.model.Payable" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'payable.label', default: 'Payable')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-payable" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-payable" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="description" title="${message(code: 'payable.description.label', default: 'Description')}" />
					
						<th><g:message code="payable.from.label" default="From" /></th>
					
						<th><g:message code="payable.to.label" default="To" /></th>
					
						<th><g:message code="payable.group.label" default="Group" /></th>
					
						<g:sortableColumn property="amount" title="${message(code: 'payable.amount.label', default: 'Amount')}" />
					
						<th><g:message code="payable.transaction.label" default="Transaction" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${payableInstanceList}" status="i" var="payableInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${payableInstance.id}">${fieldValue(bean: payableInstance, field: "description")}</g:link></td>
					
						<td>${fieldValue(bean: payableInstance, field: "from")}</td>
					
						<td>${fieldValue(bean: payableInstance, field: "to")}</td>
					
						<td>${fieldValue(bean: payableInstance, field: "group")}</td>
					
						<td>${fieldValue(bean: payableInstance, field: "amount")}</td>
					
						<td>${fieldValue(bean: payableInstance, field: "transaction")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${payableInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
--%>