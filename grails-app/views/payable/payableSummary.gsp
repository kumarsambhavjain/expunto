<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>
	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.errorMessage }">
				<div class="alert alert-error">
					<a class="close" data-dismiss="alert">×</a>
					${flash.errorMessage}
				</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class=" span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"
					params="[menuSelected:8]" />
			</div>
			<div class="span8" id="mainContent">
				<form action="#">
					<legend id="id_legend">
						<g:message code="normalized.payable"
							default="Normalized Payable" />
					</legend>
					<div class="row" id="id_inputRow">
						<div class="offset1 span1">
							<label for="group"><g:message code="for.group"
									default="For Group" /></label> <select name="group">
								<g:each var="g" in="${groups}">
									<option value="${g.id }">
										${g.groupName }
									</option>
								</g:each>
							</select>
							<button class="btn" id="id_normalize">
								<g:message code="submit" default="Submit" />
							</button>
						</div>
					</div>
				</form>
				<div id="id_normalizedResultRow">
					<!-- This area will be rendered by ajax call -->
				</div>
				<div class="row pull-right">
						<button class="btn btn-danger" id="id_save"
							style="display: none; width: 80px" data-toggle="modal"
							data-target="#id_confirmation">
							<g:message code="save" default="Save" />
						</button>
				</div>
				<div id="id_confirmation" class="modal hide fade">
					<div class="modal-header">
						<a class="close" data-dismiss="modal">×</a>
						<h2><g:message code="confirmation" default="Confirmation"/></h2>
					</div>
					<div class="modal-body">
						<h3 id="id_infoMessage"></h3>
						<div id="id_ctable"></div>
						<div class="controls" id="id_commentsInputArea">
							<label for="comment">
								<g:message code="comments"	default="Comments"/>*
							</label>
							<textarea name="comment" style="width: 520px;height: 100px;" draggable="false" contenteditable="true" autofocus="autofocus" placeholder="Your comments..."></textarea>
						</div>
						<p id="id_cmessage">
							
						</p>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn" id="id_closeButton">Close</a> <a href="#" id="id_saveButton"
							class="btn btn-primary">Save</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-modal.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="utility.js"></g:javascript>

	<script type="text/javascript">
		$(document).ready(function() {

			$("textarea[name=comment]").focus(function(){
				$('#id_commentsInputArea').attr('class','control')
			});

			$('#id_closeButton').click(function(event){
				event.preventDefault();
				$('#id_confirmation').modal('hide');
			});

			$('#id_saveButton').click(function(event){
				event.preventDefault();
				var idiv = $('#id_commentsInputArea');
				var comment = $("textarea",$(idiv)).val().trim();
				if(comment==null||comment.length==0){
					idiv.attr('class','control-group error');
					return;
				}
				// Perform Save operation here.
				$.ajax({
					type : "post",
					url : "saveNormalizedPayables?message="+comment,
					data : $('select[name="group"]').serializeArray(),
					success : function(result) {
								window.location.replace("../home");
							  },
					error : function(error) {
						//console.log(error);
						showMessage('Internal Error.Retry.', 'error');
						$('#id_normalize').hide();
						$('#id_save').hide();
					}
				});
				$('#id_confirmation').modal('hide');
			});

			$('#id_confirmation').on('shown', function () {
				$('#id_ctable').html($('#id_normalizedResultRow').html());
				$("legend",'#id_ctable').remove();
				var selectedGroup = $('select[name=group]>option:selected').html().trim();
				$('#id_infoMessage').html("For Group '"+selectedGroup+"'");
			});



		

			$('#id_normalize').click(function(event) {
				event.preventDefault();
				$('#id_save').hide();
				$('#id_normalizedResultRow').html('');
				$('#id_normalizedResultRow').addClass('loading');
				$.ajax({
					type : "post",
					url : "../payable/normalisedPayable",
					data : $('select[name="group"]').serializeArray(),
					success : function(result) {
								$('#id_normalizedResultRow').removeClass('loading');
								$('#id_normalizedResultRow').html(result);
								$('#id_save').show();
							  },
					error : function(error) {
								$('#id_normalizedResultRow').removeClass('loading');
								showMessage('Internal Error.Retry234234.', 'error');
							}
				});
			});
		});
	</script>
</body>
</html>
