
<%@ page import="com.samsoft.em.model.Payable" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>
	<g:include view="/nav/topBar.gsp"/>
	<div class="container-fluid">
		<g:if test="${flash.message }">
			<div class="row-fluid">
				<div class="span12">
					<div class="alert alert-success">
							<a class="close" data-dismiss="alert">×</a>
							${flash.message }
					</div>
				</div>
			</div>
		</g:if>
		<div class="row-fluid">
			<div class="span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"  params="[menuSelected:params.menuSelected]"/>
			</div>
			<div class=" span8" id="mainContent">
				<g:form class="form-horizontal">
					<fieldset>
						<legend>
							<g:message code="payable.details" default="Payable Details" />
						</legend>
						
						<div class="control-group">
							<label class="control-label attribute"> 
								<g:message	code="from" default="From" />
							</label>
							<div class="controls" style="padding: 4px">
									<g:link controller="user" action="show" params="[id:payableInstance.from.id]">${payableInstance.from.fname }</g:link>
							</div>
						</div>
						
						
						<div class="control-group" style="padding: 4px">
							<label class="control-label attribute"> 
								<g:message	code="to" default="To" />
							</label>
							<div class="controls"  style="padding: 4px">
								<g:link controller="user" action="show" params="[id:payableInstance.to.id]">${payableInstance.to.fname }</g:link>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label attribute"> 
								<g:message	code="amount" default="Amount" />
							</label>
							<div class="controls">
								<span class="input-xlarge uneditable-input">
									${payableInstance.amount}
								</span>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label attribute"> 
								<g:message	code="description" default="Description" />
							</label>
							<div class="controls">
									<textarea rows="" cols="">${payableInstance.description }</textarea>
							</div>
						</div>
						
						
						<div class="control-group">
							<label class="control-label attribute"> 
								<g:message	code="date.created" default="Date Created" />
							</label>
							<div class="controls">
								<span class="input-xlarge uneditable-input">
									<g:formatDate date="${payableInstance.dateCreated }"/>
								</span>
							</div>
						</div>
						
						
						
						
						
						
						
						
						
						
						
					</fieldset>
				</g:form>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-modal.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-scrollspy.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-tooltip.js"></g:javascript>
	<g:javascript src="bootstrap-popover.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>
	<g:javascript src="bootstrap-carousel.js"></g:javascript>
	<g:javascript src="bootstrap-typeahead.js"></g:javascript>

	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>

</body>
</html>
