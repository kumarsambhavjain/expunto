<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">

<%--<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="${resource(file:'apple-touch-icon-72-precomposed.png',dir:'images') }">
<link rel="apple-touch-icon-precomposed"
	href="${resource(file:'apple-touch-icon-57-precomposed.png',dir:'images') }">
--%>
</head>

<body>

	<g:include view="/nav/topBar.gsp"/>
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.errorMessage }">
					<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			${flash.errorMessage}
					</div>
			</g:if>
			<g:if test="${flash.message }">
					<div class="alert alert-success">
			  			<a type="button" class="close" data-dismiss="alert">×</a>
			  			${flash.message}
					</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class=" span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"  params="[menuSelected:6]"/>
			</div>
			<div class="span8" id="mainContent">
				<g:form class="form-horizontal" method="post" action="save">
					<g:hiddenField name="menuSelected" id="menuSelected"
						value="${params.menuSelected}" />
					<fieldset>
						<legend>
							<g:message code="add.payable"
								default="Add Payable" />
						</legend>

						<div class="control-group">
							<label class="control-label" for="group"><g:message
									code="group" default="Group" />*</label>
							<div class="controls">
								<select name="group" id="id_group" autofocus="autofocus">
									<option value="-1" disabled="disabled" selected="selected"><g:message code="choose.one" default="Choose one"/></option>
									<g:each in="${groups }" var="g">
										<option value="${g.id }">${g.groupName }</option>
									</g:each>
								</select>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="from"><g:message
									code="from" default="From" />*</label>
							<div class="controls">
								<select name="from" id="id_fromList" disabled="disabled">
									<option value="-1" disabled="disabled" selected="selected"><g:message code="from" default="From"/></option>
								</select>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="to"><g:message
									code="to" default="To" />*</label>
							<div class="controls">
								<select name="to" id="id_toList">
									<option  selected="selected" value="${currentUser.id }">${currentUser.fname }</option>
								</select>
							</div>
						</div>
						
						
						<div class="control-group">
							<label class="control-label" for="amount"><g:message
									code="amount" default="Amount" />*</label>
							<div class="controls">
								<input type="number" name="amount" required="required" size="8" min="1" max="999999999"/>
							</div>
						</div>
						
						
						<div class="control-group">
							<label class="control-label" for="comments"><g:message
									code="comments" default="Comments" />*</label>
							<div class="controls">
								<textarea class="input-xlarge" id="textarea" rows="3" name="description" required="required"></textarea>
							</div>
						</div>
						
						

						<div class="form-actions">
							<input type="submit" class="btn btn-primary" value="Add" id="submitButton" /> 
							<input type="button" class="btn btn-danger" value="Cancel" />
						</div>

					</fieldset>
				</g:form>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="utility.js"></g:javascript>
	<script type="text/javascript">
		$(document).ready(function() {
			$('form').submit(function(event){
				if($('#id_group option:selected').attr('value')=='-1'){
					showMessage('Select Group','error');
					$('#id_group').focus();
					event.preventDefault();
					return;
				}

				var from = $('#id_fromList>option:selected').attr('value');
				var to = $('#id_toList>option:selected').attr('value');
				if(from==to){
					showMessage('Select different users.','error');
					$('#id_group').focus();
					return;
				}


				$('form').submit();
			});
			
			$('#id_group').change(function(){
				var grp = $('#id_group').val();
				if(grp){
					$('select').removeAttr('disabled');
					$.ajax({
						type	:		"post",
						data	:		$('#id_group').serializeArray(),
						url		:		"getGroupUsers",
						cache	:		false,
						success	:		function(response){
											$('#id_fromList').html('');
											for(var i =0;i<response.length;i++){
												$('#id_fromList').append('<option value="'+response[i][0]+'">'+response[i][1]+' '+response[i][2]+'</option>');
											}
										},
						error	:		function(error){
										}
					});
				}
			});
		});
	</script>

</body>
</html>
