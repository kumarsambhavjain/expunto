<!DOCTYPE html>
<html lang="en">
<body>
	<div>
		<div>
			<div>
				<table style="margin-top: 30px;border: medium;border-color: black;">
					<thead>
						<tr class="headerRow" style="background-color: #0088cc;">
							<th style="font-weight: bolder; color: white; margin: 15%"><g:message
									code="from" default="From" /></th>
							<th style="font-weight: bolder; color: white; margin: 15%"><g:message
									code="to" default="To" /></th>
							<th style="font-weight: bolder; color: white; margin: 15%"><g:message
									code="amount" default="Amount" /></th>
							<th style="font-weight: bolder; color: white; margin: 15%"><g:message
									code="Description" default="Description" /></th>
						</tr>
					</thead>
					<tbody>
						<g:if test="${payableInstance==null}">
							<tr id="id_noResult">
								<td colspan="7"><g:message code="no.results.found"
										default="No Results Found" /></td>
							</tr>
						</g:if>
						<g:else>
							<tr>
								<td style="background-color: #f9f9f9;padding-left: 15%;padding-right: 15%;">
									${payableInstance.from.fname}
								</td>
								<td style="background-color: #f9f9f9;padding-left: 15%;padding-right: 15%;">
									${payableInstance.to.fname}
								</td>
								<td style="background-color: #f9f9f9;padding-left: 15%;padding-right: 15%;"><g:formatNumber
										number="${payableInstance.amount }" type="number" /></td>
								<td style="background-color: #f9f9f9;padding-left: 15%;padding-right: 15%;">
									${payableInstance.description }
								</td>
							</tr>
						</g:else>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>