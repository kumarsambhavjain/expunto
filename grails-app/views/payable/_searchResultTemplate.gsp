<table class="table table-striped" style="margin-top: 30px">
	<thead style="table-layout: fixed;">
		<tr class="headerRow">
			<th><g:message code="date" default="Date" /></th>
			<th><g:message code="date" default="From" /></th>
			<th><g:message code="date" default="To" /></th>
			<th><g:message code="date" default="Amount" /></th>
			<th><g:message code="date" default="Status" /></th>
			<th><g:message code="date" default="Description" /></th>
			<th><g:message code="created.by" default="Created By" /></th>
			<!-- <th></th> -->
		</tr>
	</thead>
	<tbody id="id_payableSearchResult">
		<g:if test="${result==null || result.size==0 }">
			<tr>
				<td colspan="7">
					<g:message code="no.results.found" default="No Results Found"/>
				</td>
			</tr>
		</g:if>
		<g:else>
			<g:each in="${result}" var="p">
				<tr id="p${p.id}">
					<td><g:formatDate date="${p.dateCreated }" /></td>
					<td><g:link controller="user" action="show"
							params="[id:p.from.id]">
							${p.from.fname}
						</g:link></td>
					<td><g:link controller="user" action="show"
							params="[id:p.to.id]">
							${p.to.fname}
						</g:link></td>
					<td>
						<g:formatNumber number="${p.amount }" type="number"/>
					</td>
					<td>
						<g:if test="${p.transaction }">
							<g:message code="paid" default="Paid"/>
						</g:if>
						<g:else>
							<g:message code="unpaid" default="Unpaid"/>
						</g:else>
					</td>
					<td width="20%">
						${p.description }
					</td>
					<td><g:link controller="user" action="show"
							params="[id:p.createdBy.id]">
							${p.createdBy.fname}
						</g:link></td>
					<!-- <td>
						<div class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><g:message
									code="action" default="Action" /> <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<g:if test="${p.transaction==null }">
									<li><a href="#"><g:message code="mark.paid"
												default="Mark Paid" /></a></li>
								</g:if>
							</ul>
						</div>
					</td> -->
				</tr>
			</g:each>
			<tr>
				<td></td>
				<td></td>
				<td style="font-weight: 900;font-size: large;"><g:message code="total" default="Total"/></td>
				<td style="font-weight: 900;font-size: large;">
					<g:formatNumber number="${total }" type="number"/>
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</g:else>
	</tbody>
</table>