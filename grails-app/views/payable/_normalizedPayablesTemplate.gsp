<form>
	<legend id="id_legend">
		<g:message code="result" default="Result" />
	</legend>
	<table class="table table-striped" style="margin-top: 30px">
		<thead style="table-layout: fixed;">
			<tr class="headerRow">
				<th><g:message code="from" default="From" /></th>
				<th><g:message code="to" default="To" /></th>
				<th><g:message code="amount" default="Amount" /></th>
			</tr>
		</thead>
		<tbody id="id_payableSearchResult">
			<g:if test="${result==null || result.size==0 }">
				<tr id="id_noResult">
					<td colspan="7"><g:message code="no.results.found"
							default="No Results Found" /></td>
				</tr>
			</g:if>
			<g:else>
				<g:each in="${result}" var="p">
					<tr>
						<td><g:link controller="user" action="show"
								params="[id:p.from.id]">
								${p.from.getFullName()}
							</g:link></td>
						<td><g:link controller="user" action="show"
								params="[id:p.to.id]">
								${p.to.getFullName()}
							</g:link></td>
						<td><g:formatNumber number="${p.amount }" type="number" /></td>
					</tr>
				</g:each>
			</g:else>
		</tbody>
	</table>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$('#id_normalize').click(function(event) {
			event.preventDefault();
			$('#id_save').hide();
			$('#id_normalizedResultRow').html('');
			$('#id_normalizedResultRow').addClass('loading');
			$.ajax({
				type : "post",
				url : "../payable/normalisedPayable",
				data : $('select[name="group"]').serializeArray(),
				success : function(result) {
							$('#id_normalizedResultRow').removeClass('loading');
							$('#id_normalizedResultRow').html(result);
							$('#id_save').show();
						  },
				error : function(error) {
							$('#id_normalizedResultRow').removeClass('loading');
							showMessage('Internal Error.Retry234234.', 'error');
						}
			});
		});
	});
</script>