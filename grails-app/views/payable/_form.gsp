<%@ page import="com.samsoft.em.model.Payable" %>



<div class="fieldcontain ${hasErrors(bean: payableInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="payable.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${payableInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payableInstance, field: 'from', 'error')} required">
	<label for="from">
		<g:message code="payable.from.label" default="From" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="from" name="from.id" from="${com.samsoft.em.model.User.list()}" optionKey="id" required="" value="${payableInstance?.from?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payableInstance, field: 'to', 'error')} required">
	<label for="to">
		<g:message code="payable.to.label" default="To" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="to" name="to.id" from="${com.samsoft.em.model.User.list()}" optionKey="id" required="" value="${payableInstance?.to?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payableInstance, field: 'group', 'error')} required">
	<label for="group">
		<g:message code="payable.group.label" default="Group" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="group" name="group.id" from="${com.samsoft.em.model.UserGroup.list()}" optionKey="id" required="" value="${payableInstance?.group?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payableInstance, field: 'amount', 'error')} required">
	<label for="amount">
		<g:message code="payable.amount.label" default="Amount" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="amount" min="1" max="500000" required="" value="${fieldValue(bean: payableInstance, field: 'amount')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payableInstance, field: 'transaction', 'error')} ">
	<label for="transaction">
		<g:message code="payable.transaction.label" default="Transaction" />
		
	</label>
	<g:select id="transaction" name="transaction.id" from="${com.samsoft.em.model.Transaction.list()}" optionKey="id" value="${payableInstance?.transaction?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payableInstance, field: 'createdBy', 'error')} required">
	<label for="createdBy">
		<g:message code="payable.createdBy.label" default="Created By" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="createdBy" name="createdBy.id" from="${com.samsoft.em.model.User.list()}" optionKey="id" required="" value="${payableInstance?.createdBy?.id}" class="many-to-one"/>
</div>

