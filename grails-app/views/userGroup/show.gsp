<%@ page import="com.samsoft.em.model.UserGroup"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>

	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<g:if test="${flash.message }">
			<div class="row-fluid">
				<div class="span12">
					<div class="alert alert-success">
						<a class="close" data-dismiss="alert">×</a>
						${flash.message }
					</div>
				</div>
			</div>
		</g:if>
		<div class="row-fluid">
			<div class="span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"  params="[menuSelected:params.menuSelected]"/>
			</div>
			<div class=" span8" id="mainContent">
				<g:form class="form-horizontal">
					<fieldset>
						<legend>
							<g:message code="group.details"
								default="Group Details" />
						</legend>
						
						
						<g:if test="${userGroupInstance?.groupName}">						
							<div class="control-group">
								<label class="control-label attribute">
									<g:message code="group.name" default="Group Name"/>
								</label>
								<div class="controls">
									<span class="input-xlarge uneditable-input">${userGroupInstance.groupName }</span>
								</div>
							</div>
						</g:if>
						<g:if test="${userGroupInstance?.groupKey}">
						<div class="control-group">
							<label class="control-label attribute">
								<g:message code="group.key" default="Group Key"/>
							</label>
							<div class="controls">
								<span class="input-xlarge uneditable-input">${userGroupInstance?.groupKey}</span>
							</div>
						</div>
						</g:if>
						
						<g:if test="${userGroupInstance?.createdBy}">
						<div class="control-group">
							<label class="control-label attribute">
								<g:message code="group.created.by" default="Created By"/>
							</label>
							<div class="controls">
								<span class="input-xlarge uneditable-input">${userGroupInstance?.createdBy.fname}</span>
							</div>
						</div>
						</g:if>
						
						<g:if test="${userGroupInstance?.dateCreated}">
						<div class="control-group">
							<label class="control-label attribute">
								<g:message code="create.date" default="Create Date"/>
							</label>
							<div class="controls">
								<span class="input-xlarge uneditable-input"><g:formatDate date="${userGroupInstance?.dateCreated}"/></span>
							</div>
						</div>
						</g:if>
						
						<g:if test="${userGroupInstance?.moderator}">
						<div class="control-group">
							<label class="control-label attribute">
								<g:message code="group.moderator attribute" default="Moderator"/>
							</label>
							<div class="controls">
								<span class="input-xlarge uneditable-input">${userGroupInstance?.moderator.fname}</span>
							</div>
						</div>
						</g:if>
						
						<g:if test="${userGroupInstance}">
						<div class="control-group">
							<label class="control-label attribute">
								<g:message code="group.members attribute" default="Members"/>
							</label>
							<div class="controls" style="padding: 4px">
									<g:each var="membership" in="${userGroupInstance?.memberships}">
											<g:link controller="user" action="show" params="[id:membership.user.id]">${membership.user.fullName }</g:link>,
									</g:each>
							</div>
						</div>
						</g:if>
					</fieldset>
				</g:form>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>
	<g:javascript src="bootstrap-carousel.js"></g:javascript>

	<script type="text/javascript">
		$(document).ready(function() {
			$('.alert').slideDown("slow");
		});
	</script>

</body>
</html>
