<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">

<%--<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="${resource(file:'apple-touch-icon-72-precomposed.png',dir:'images') }">
<link rel="apple-touch-icon-precomposed"
	href="${resource(file:'apple-touch-icon-57-precomposed.png',dir:'images') }">
--%>
</head>

<body>

	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<div class="row-fluid">
			<div class=" span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"  params="[menuSelected:1]"/>
			</div>
			<div class="span8" id="mainContent">
				<g:form class="form-horizontal" method="post" action="save">
					<fieldset>
						<legend>
							<g:message code="user.create.new.group"
								default="Create New Group" />
						</legend>

						<div class="control-group">
							<label class="control-label" for="groupName"><g:message
									code="group.groupname" default="Group Name" />*</label>
							<div class="controls">
								<input title="Group Name" type="text" class="input-xlarge focused" id="groupName"
									name="groupName" required="required" autofocus="autofocus"
									autocomplete="on" placeholder="New Group Name"> <span
									class="help-inline"></span>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label" for="includeExisting"><g:message
									code="include.existing.members"
									default="Include existing members?" /></label>
							<div class="controls">
								<label class="radio"> <input type="radio"
									name="includeExisting" id="includeExisting" value="true">
									<g:message code="yes" default="Yes" />
								</label> <label class="radio"> <input type="radio"
									name="includeExisting" id="includeExisting2" value="false"
									checked /> <g:message code="no" default="No" />
								</label>
							</div>
						</div>

						<div class="control-group" id="listDiv">
							<label class="control-label" for="memberList"> <g:message
									code="group.member" default="Members" />
							</label>

							<div class="controls">
								<select multiple="multiple" name="memberList" id="memberList">
									
								</select>
							</div>
						</div>

						<div class="form-actions">
							<input type="submit" class="btn btn-primary" value="Create"
								id="submitButton" /> <input type="button"
								class="btn btn-danger" value="Cancel" />
						</div>

					</fieldset>
				</g:form>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#listDiv').hide();

			$('#includeExisting').click(function() {
				
				$('#memberList').html('');
				$.ajax({
					type : "post",
					cache:	true,
					url : "./getCoUsers",
					success : function(result) {
						if (result) {
							$('#listDiv').show(150);
							if(result.length>0){
								$('#memberList').attr('required','required');
								for(var i=0;i<result.length;i++){
									$('#memberList').append('<option value="'+result[i].id+'">'+result[i].fname+'</option>');
								}
							}else{
								// there are no co users.
								$('#memberList').removeAttr('required');
								$('#memberList').append('<option disabled="disabled">No users found</option>');
							}
						}
					},
					error : function(response) {
								//console.log(response);
								showMessage('Internal Error.Retry.','error');
							}
				});	


						
			});
			$('#includeExisting2').click(function() {
				$('#listDiv').hide(150);
				$('#memberList').removeAttr('required');
			});
		});
	</script>

</body>
</html>
