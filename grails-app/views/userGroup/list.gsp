<%@ page import="com.samsoft.em.model.UserGroup" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
	<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">

<%--<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="${resource(file:'apple-touch-icon-72-precomposed.png',dir:'images') }">
<link rel="apple-touch-icon-precomposed"
	href="${resource(file:'apple-touch-icon-57-precomposed.png',dir:'images') }">
--%>
</head>

<body>

	<g:include view="/nav/topBar.gsp"/>
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.errorMessage }">
					<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			${flash.errorMessage}
					</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class="span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"  params="[menuSelected:3]"/>
			</div>
			<div class=" span8" id="mainContent">
				<form>
						<legend id="id_legend">
								<g:message code="groups" default="Groups"/>
						</legend>
				
				<table  class="table table-striped">
					<thead>
						<tr class="headerRow">
							<g:sortableColumn property="groupName" title="${message(code: 'userGroup.groupName.label', default: 'Group Name')}" />
							<th><g:message code="userGroup.createdBy.label" default="Created By" /></th>
							<th><g:message code="userGroup.moderator.label" default="Moderator" /></th>
							<g:sortableColumn property="groupKey" title="${message(code: 'userGroup.groupKey.label', default: 'Group Key')}" />
							<g:sortableColumn property="dateCreated" title="${message(code: 'userGroup.dateCreated.label', default: 'Date Created')}" />
						</tr>
					</thead>
					<tbody>
					<g:if test="${userGroupInstanceList?.size==0 }">
						<tr>
							<td colspan="5">
								<g:message code="no.group.created" default="No Group created."/>
								<g:link class="navi"  controller="userGroup"   action="create"><g:message code="create" default="Create"/></g:link>
							</td>
						</tr>
					</g:if>
					<g:else>
						<g:each in="${userGroupInstanceList}" status="i" var="userGroupInstance">
							<tr>
								<td><g:link action="show" id="${userGroupInstance.id}">${fieldValue(bean: userGroupInstance, field: "groupName")}</g:link></td>
								<td>${fieldValue(bean: userGroupInstance, field: "createdBy.fname")}</td>
								<td>${fieldValue(bean: userGroupInstance, field: "moderator.fname")}</td>
								<td>${fieldValue(bean: userGroupInstance, field: "groupKey")}</td>
								<td><g:formatDate date="${userGroupInstance.dateCreated}" /></td>
							</tr>
						</g:each>
					</g:else>
					</tbody>
				</table>
				</form>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-modal.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-scrollspy.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-tooltip.js"></g:javascript>
	<g:javascript src="bootstrap-popover.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>
	<g:javascript src="bootstrap-carousel.js"></g:javascript>
	<g:javascript src="bootstrap-typeahead.js"></g:javascript>

	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>

</body>
</html>
