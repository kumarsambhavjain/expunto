<%@ page import="com.samsoft.em.model.UserGroup" %>



<div class="fieldcontain ${hasErrors(bean: userGroupInstance, field: 'groupName', 'error')} required">
	<label for="groupName">
		<g:message code="userGroup.groupName.label" default="Group Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="groupName" required="" value="${userGroupInstance?.groupName}"/>
</div>
