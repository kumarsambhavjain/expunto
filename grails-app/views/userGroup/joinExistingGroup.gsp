<!DOCTYPE html>
<%
		String hadd  = null;
 %>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">

<%--<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="${resource(file:'apple-touch-icon-72-precomposed.png',dir:'images') }">
<link rel="apple-touch-icon-precomposed"
	href="${resource(file:'apple-touch-icon-57-precomposed.png',dir:'images') }">
--%>
</head>

<body>
	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
				<g:if test="${flash.errorMessage }">
					<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			${flash.errorMessage}
					</div>
				</g:if>
		<div class="row-fluid">
			<div class=" span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"  params="[menuSelected:2]"/>
			</div>
			<div class=" span8" id="mainContent">
				<g:form class="form-horizontal" method="post"  action="addUserToGroup">
					<fieldset>
						<legend>
							<g:message code="join.existing.group"
								default="Join Existing Group" />
						</legend>

						<div class="control-group">
							<label class="control-label" for="groupkey"><g:message
									code="group.key" default="Group Key" />*</label>
							<div class="controls">
								<input type="text" class="input-xlarge focused" id="groupkey"
									title="Unique group key known to its members." name="groupkey"
									required="required" autofocus="autofocus" autocomplete="on"
									placeholder="Group Key"> <span class="help-inline"></span>
							</div>
						</div>

						<div class="form-actions">
							<input type="submit" class="btn btn-primary" value="Join"
								id="submitButton" /> <input type="button" class="btn btn-danger"
								value="Cancel" />
						</div>
					</fieldset>
				</g:form>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-modal.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-scrollspy.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-tooltip.js"></g:javascript>
	<g:javascript src="bootstrap-popover.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>
	<g:javascript src="bootstrap-carousel.js"></g:javascript>
	<g:javascript src="bootstrap-typeahead.js"></g:javascript>

	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>

</body>
</html>
