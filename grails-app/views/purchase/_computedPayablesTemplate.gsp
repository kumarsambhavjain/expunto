<form method="post" id="id_form">
	<input type="hidden" name="group" id="id_group"/>
	<table class="table table-striped" style="margin-top: 30px">
		<thead style="table-layout: fixed;">
			<tr class="headerRow">
				<th><g:message code="date" default="From" /></th>
				<th><g:message code="date" default="To" /></th>
				<th><g:message code="date" default="Amount" /></th>
			</tr>
		</thead>
		<tbody id="id_payable">
			<g:if test="${result==null || result.size==0 }">
				<tr>
					<td colspan="3"><g:message code="no.payables"
							default="No Payables" /></td>
				</tr>
			</g:if>
			<g:else>
				<g:each in="${result}" var="p">
					<tr id="p${p.id}">
						<td>
							${p.from.fullName}
						</td>
						<td>
							${p.to.fullName}
						</td>
						<td><g:formatNumber number="${p.amount }" type="number" /></td>
					</tr>
				</g:each>
				<tr>
					<td></td>
					<td style="font-weight: 900; font-size: large;"><g:message
							code="total" default="Total" /></td>
					<td style="font-weight: 900; font-size: large;"><g:formatNumber
							number="${total }" type="number" /></td>
				</tr>
			</g:else>
		</tbody>
	</table>
	<div class="row pull-right">
		<input type="button" class="btn btn-primary" value="Save" id="id_saveButton"/>
		<input type="button" class="btn btn-danger" value="Cancel" id="id_cancelButton"/>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		
		$("#id_indCostDiv").show();
		$("#id_indvCost").html('<g:formatNumber number="${individualCost }" type="number" />');
		$("#id_group").attr("value",$("#id_groupSelect>option:selected").val());
		$("#id_saveButton").click(function(){
			$("#id_form").attr("action","savePurchase");
			$("#id_form").submit();
		});

		$("#id_cancelButton").click(function(){
			$("#id_form").attr("action","create");
			$("#id_form").submit();
		});
	});
</script>