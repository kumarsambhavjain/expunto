<%@ page import="com.samsoft.em.model.Purchase" %>



<div class="fieldcontain ${hasErrors(bean: purchaseInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="purchase.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${purchaseInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseInstance, field: 'dateOfPurchase', 'error')} required">
	<label for="dateOfPurchase">
		<g:message code="purchase.dateOfPurchase.label" default="Date Of Purchase" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="dateOfPurchase" precision="day"  value="${purchaseInstance?.dateOfPurchase}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseInstance, field: 'totalCost', 'error')} required">
	<label for="totalCost">
		<g:message code="purchase.totalCost.label" default="Total Cost" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="totalCost" min="1" required="" value="${fieldValue(bean: purchaseInstance, field: 'totalCost')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseInstance, field: 'group', 'error')} required">
	<label for="group">
		<g:message code="purchase.group.label" default="Group" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="group" name="group.id" from="${com.samsoft.em.model.UserGroup.list()}" optionKey="id" required="" value="${purchaseInstance?.group?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseInstance, field: 'beneficiaries', 'error')} ">
	<label for="beneficiaries">
		<g:message code="purchase.beneficiaries.label" default="Beneficiaries" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${purchaseInstance?.beneficiaries?}" var="b">
    <li><g:link controller="beneficiary" action="show" id="${b.id}">${b?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="beneficiary" action="create" params="['purchase.id': purchaseInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'beneficiary.label', default: 'Beneficiary')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: purchaseInstance, field: 'createdBy', 'error')} required">
	<label for="createdBy">
		<g:message code="purchase.createdBy.label" default="Created By" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="createdBy" name="createdBy.id" from="${com.samsoft.em.model.User.list()}" optionKey="id" required="" value="${purchaseInstance?.createdBy?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseInstance, field: 'items', 'error')} ">
	<label for="items">
		<g:message code="purchase.items.label" default="Items" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${purchaseInstance?.items?}" var="i">
    <li><g:link controller="item" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="item" action="create" params="['purchase.id': purchaseInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'item.label', default: 'Item')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: purchaseInstance, field: 'payables', 'error')} ">
	<label for="payables">
		<g:message code="purchase.payables.label" default="Payables" />
		
	</label>
	<g:select name="payables" from="${com.samsoft.em.model.Payable.list()}" multiple="multiple" optionKey="id" size="5" value="${purchaseInstance?.payables*.id}" class="many-to-many"/>
</div>

