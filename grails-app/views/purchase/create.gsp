<%@ page import="com.samsoft.em.model.Purchase"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="sambhav">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />
</head>

<body>

	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.errorMessage }">
					<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			${flash.errorMessage}
					</div>
			</g:if>
			<g:if test="${flash.message }">
					<div class="alert alert-success">
			  			<a type="button" class="close" data-dismiss="alert">×</a>
			  			${flash.message}
					</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class="span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"
					params="[menuSelected:4]" />
			</div>

			<div class="span8" id="mainContent">
				<div class="row-fluid">
					<legend  id="id_legend">
						<g:message code="new.purchase" default="New Purchase" />
					</legend>
					<div class="alert alert-info">
						<div class="row">
							<div class="span2">
								<h4>Total Cost: <span id="id_totalCost">0</span></h4>
							</div>
							<div class="span3 offset1">
								<h4>Total Contribution: <span id="id_totalContri">0</span></h4>
							</div>
							<div class="span2 offset0" style="display: none;" id="id_indCostDiv">
								<h4>Individual Cost: <span id="id_indvCost">0</span></h4>
							</div>
						</div>
					</div>
					<div class="tabbable"> <!-- Only required for left/right tabs -->
					  <ul class="nav nav-tabs">
					    <li class="active"><a href="#tab1" data-toggle="tab"><g:message code="items" default="Items" /></a></li>
					    <li><a href="#tab2" data-toggle="tab"><g:message code="beneficiaries" default="Beneficiaries" /></a></li>
					    <li><a href="#tab3" data-toggle="tab"><g:message code="payables" default="Payables" /></a></li>
					  </ul>
					  <div class="tab-content">
					    <div class="tab-pane active" id="tab1">
						    <div class="row-fluid offset0" style="margin-top: 30px">
								<input name="item" autofocus="autofocus" style="width: 135px" autocomplete="on" id="id_Item"
									placeholder="<g:message code="item" default="Item"/>"
									class="input-small" /> <input name="price" id="id_price"
									placeholder="<g:message code="price" default="Price"/>"
									class="input-small" type="number" min="1" maxlength="6"
									size="6" max="999999" />
								<button class="btn " style="margin-bottom: 9px"
									id="id_btnAdd">
									<g:message code="add.item" default="Add Item" />
								</button>
							</div>
							<div class="row-fluid" style="margin-top: 20px">
								<table class="table table-striped" id="id_itemTable">
									<thead>
										<tr class="headerRow">
											<th style="width: 40%">Item</th>
											<th style="width: 40%">Cost</th>
											<th style="width: 20%"><a href="#"  id="id_removeAllItem">x</a> </th>
										</tr>
									</thead>
									<tbody>
										<tr id="id_NoDataRow">
											<td colspan="3">
												<g:message code="no.items.added" default="No Items Added" />
											</td>
										</tr>
									</tbody>
									<tfoot>
										<tr style="display: none;" id="id_totalRow">
											<td style="font-weight: 900; font-size: large;">Total Cost</td>
											<td style="font-weight: 900; font-size: large;" id="id_total" colspan="2"></td>
										</tr>
									</tfoot>
								</table>
							</div>
					    </div>
					    <div class="tab-pane" id="tab2">
					      <div class="row-fluid offset0"  style="margin-top: 10px">
					      	<div class="span3">
					      		<label for="group">Select a group</label>
								<select name="group" id="id_groupSelect" class="span2" style="width: 100%"> 
									<option disabled="disabled" value="-1" selected="selected"><g:message code="choose.group" default="Choose Group"/></option>
									<g:each in="${groups}" var="g">
										<option value="${g.id }">${g.groupName }</option>
									</g:each>
								</select>				
							</div>
						  </div>
						  <div class="row-fluid" id="id_BenfTable">
						  	
						  </div>
					    </div>
					    <div class="tab-pane" id="tab3">
					      
					    </div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /container -->
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="utility.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<script type="text/javascript">
		var totalCost = 0;
		var totalContribution = 0;
		$(document).ready(function(){
			$("ul.nav-tabs>li:first").addClass("active");

			//add item here.
			$("#id_btnAdd").click(function(event){
				var item = $("#id_Item").val().trim();
				if(item.length==0){
					event.preventDefault();
					return;
				}
				var price = $("#id_price").val().trim();
				if(price.length==0||isNaN(price)){
					$("#id_price").val("");
					event.preventDefault();
					return;
				}
				$("#id_Item").val("");
				$("#id_price").val("");
				$("#id_NoDataRow").hide();
				$("#id_totalRow").show();
				price = parseInt(price);
				totalCost+=price;
				$("#id_NoDataRow").parent().append("<tr class=itemx>  <td>"+item+"</td>    <td>"+price+"</td>  <td><a href='#'  onclick=removeItem(this)>x</a>    </td>  </tr>");
				$("#id_total").html(totalCost);
				$("#id_totalCost").html(totalCost);
				
			});

			//remove all items
			$("#id_removeAllItem").click(function(){
				$("tbody>tr:visible",$("#id_itemTable")).remove();
				$("#id_NoDataRow").show();
				$("#id_totalRow").hide();
				totalCost =0;
				$("#id_totalCost").html(0);
			});

			//group select handled here
			$("#id_groupSelect").change(function(){
				$.ajax({
					type	:	"post",
					url		:	"renderGroupBeneficiaries?group="+$("option:selected",$(this)).val(),
					cache	:	true,
					context	:	$("#id_BenfTable"),
					dataType:	"html",
					success	:	function(response){
									$("#id_BenfTable").html(response);
									
								},
					error	:	function(error){
									alert("Error");
								}
				});
			});

			// compute payables here
			$("ul.nav-tabs>li:last").on("show",function(e){
				if(!isNaN(totalContribution)&&!isNaN(totalCost)&&totalContribution>1&&totalCost>1&&totalContribution==totalCost){
					var url = "computePayables?t=8";
					$("tr.itemx").each(function(){
						url+=("&description="+$("td:first",$(this)).html().trim()+"&cost="+$("td:nth-child(2)",$(this)).html().trim());  
					});
					$("input[name=amountContributed]:enabled").each(function(){
						var row = $(this).parent().parent(); 
						url+="&bid="+$("td:first>input[type=checkbox]",row).val()+"&amountContributed="+$("td:last>input[type=number]",row).val();
					});
					$.ajax({
						type	:	"post",
						url		:	 url,
						cache	:	false,
						dataType:	"html",
						success	:	function(response){
										$("#tab3").html(response);
									},
						error	:	function(error){
										alert("Error");
									}
					});	
				}else{
					alert('Invalid stuff');
					e.preventDefault();
					return;
				}
			});

			$("ul.nav-tabs>li:eq(1)").on("shown",function(e){
				$("#id_indCostDiv").hide();
				$("#id_groupSelect").focus();
			});
			$("ul.nav-tabs>li:eq(0)").on("shown",function(e){
				$("#id_indCostDiv").hide();
			});
		});

		function removeItem(item){
			var cost =  $("td:eq(1)", $(item).parent().parent()).html().trim(); 
			totalCost-=parseInt(cost);
			$(item).parent().parent().remove();
			if(totalCost<=0){
				$("#id_NoDataRow").show();
				$("#id_totalRow").hide();
			}else{
				$("#id_total").html(totalCost);
			}
			$("#id_totalCost").html(totalCost);
		}
	</script>

</body>
</html>
