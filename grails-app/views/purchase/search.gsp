<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>
	<g:include view="/nav/topBar.gsp"/>
	<div class="container-fluid">
		<div id="id_message">
			<g:if test="${flash.errorMessage }">
					<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			${flash.errorMessage}
					</div>
			</g:if>
		</div>
		<div class="row-fluid">
			<div class=" span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"  params="[menuSelected:params.menuSelected]"/>
			</div>
			<div class=" span8" id="mainContent">
					<form>
						<legend id="id_legend">
								<g:message code="search.purchase" default="Search Purchase"/>
						</legend>
							<div id="id_SearchArea">	
								<div class="row">
									<div class="span3 offset1">
										<label class="search-label"><g:message code="group" default="Group"/></label>
								  		<select name="groupCode" id="id_groupSelect" multiple="multiple" size="2">
							  				<g:each in="${groupSet}" var="group">
							  					<option value="${group.id}">${group.groupName }</option>
							  				</g:each>
								  		</select>
									</div>
									<div class="span3 offset1">
										<label class="search-label"><g:message code="created.by" default="Created By"/></label>
								  		<select name="fromUser" id="id_fromUser" disabled="disabled" class="userSelect" multiple="multiple">
								  		</select>
									</div>
								</div>
								<div class="row">	
									<div class="span3 offset1">
										<label class="search-label"><g:message code="from.date" default="From Date"/></label>
									</div>
									<div class="span3 offset1">
										<label class="search-label"><g:message code="to.date" default="To Date"/></label>
									</div>
					     	   </div>
					     	    <div class="row">
					     	   		<div class="span3 offset1">
					     	   				<input class="btn disabled " type="button" value="Search" id="id_SearchButton"/>
					     	   		</div>
			     	  			 </div>
				     	   </div> <!-- End of search Area -->
			     	  
					</form>
				
				<div id="resultArea">
					
				</div>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-modal.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-scrollspy.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-tooltip.js"></g:javascript>
	<g:javascript src="bootstrap-popover.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>
	<g:javascript src="bootstrap-carousel.js"></g:javascript>
	<g:javascript src="bootstrap-typeahead.js"></g:javascript>
	<g:javascript src="utility.js"></g:javascript>

	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>

</body>
</html>
