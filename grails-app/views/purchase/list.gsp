
<%@ page import="com.samsoft.em.model.Purchase" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'purchase.label', default: 'Purchase')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-purchase" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-purchase" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="description" title="${message(code: 'purchase.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="dateOfPurchase" title="${message(code: 'purchase.dateOfPurchase.label', default: 'Date Of Purchase')}" />
					
						<g:sortableColumn property="totalCost" title="${message(code: 'purchase.totalCost.label', default: 'Total Cost')}" />
					
						<th><g:message code="purchase.group.label" default="Group" /></th>
					
						<th><g:message code="purchase.createdBy.label" default="Created By" /></th>
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'purchase.dateCreated.label', default: 'Date Created')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${purchaseInstanceList}" status="i" var="purchaseInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${purchaseInstance.id}">${fieldValue(bean: purchaseInstance, field: "description")}</g:link></td>
					
						<td><g:formatDate date="${purchaseInstance.dateOfPurchase}" /></td>
					
						<td>${fieldValue(bean: purchaseInstance, field: "totalCost")}</td>
					
						<td>${fieldValue(bean: purchaseInstance, field: "group")}</td>
					
						<td>${fieldValue(bean: purchaseInstance, field: "createdBy")}</td>
					
						<td><g:formatDate date="${purchaseInstance.dateCreated}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${purchaseInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
