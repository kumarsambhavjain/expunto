<table class="table table-striped" id="id_benfTable"
	style="margin-top: 20px;">
	<thead>
		<tr class="headerRow">
			<th><input type="checkbox" id="id_checkAll" autofocus="autofocus"/></th>
			<th><g:message code="name" default="Name" /></th>
			<th><g:message code="contribution" default="Contribution" /></th>
		</tr>
	</thead>
	<tbody>
		<g:each var="m" in="${members}">
			<tr>
				<td><input type="checkbox" name="bid" value="${m[0]}" /></td>
				<td>
					${m[1]} ${m[2] }
				</td>
				<td style="padding-bottom: 0px;"><input type="number" class="input-small"
					name="amountContributed" value="0" maxlength="6" min="0" 
					max="999999" disabled="disabled" /></td>
			</tr>
		</g:each>
	</tbody>
	<tfoot>
		<tr>
			<td></td>
			<td style="font-weight: 900; font-size: large;">Total</td>
			<td style="font-weight: 900; font-size: large;" id="id_totalContribution"
				>0</td>
		</tr>
	</tfoot>
</table>
<script type="text/javascript">
	$(document).ready(function() {
	
		totalContribution = 0;
		$("input[name=bid]").change(function(){
			$("td:last>input",$(this).parent().parent()).prop("disabled",!$(this).prop("checked"));
			setTotalContribution();
		});

		$("input[name=amountContributed]").change(function(){
			setTotalContribution();
		});
		
		$("#id_checkAll").change(function() {
			var checked = $(this).prop("checked");
			var inp = null;
			$("tbody input[type=checkbox]").each(function(){
				$(this).prop("checked",checked);
				inp = $("td:last>input",$(this).parent().parent());
				inp.prop("disabled",!checked);
			});
			setTotalContribution();
		});


		
	});
	function setTotalContribution(){
		var total = 0;
		$("input[name=amountContributed]:enabled").each(function(){
			total+= (parseInt($(this).val())||0);
		});
		$("#id_totalContribution").html(total);
		totalContribution = total;
		$("#id_totalContri").html(total);
		if(total==totalCost){
			//alert("set hai mamla");
		}
		return total;
	}
</script>
