<%@ page import="com.samsoft.em.model.UserGroup"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>

	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<g:if test="${flash.message }">
			<div class="row-fluid">
				<div class="span12">
					<div class="alert alert-success">
						<a class="close" data-dismiss="alert">×</a>
						${flash.message }
					</div>
				</div>
			</div>
		</g:if>
		<div class="row-fluid">
			<div class="span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"  params="[menuSelected:params.menuSelected]"/>
			</div>
			<div class=" span8" id="mainContent">
				<legend>
					<g:message code="purchase.details"
						default="Purchase Details" />
				</legend>
				<g:if test="${purchaseInstance }">
					<div class="row-fluid">
						
						<table class="table table-striped">
							<thead style="table-layout: fixed;">
								<tr class="headerRow">
									<th><g:message code="date" default="Date" /></th>
									<th><g:message code="created.by" default="Created By" /></th>
									<th><g:message code="total.cost" default="Total Cost" /></th>
									<th><g:message code="individual.cost" default="Individual Cost" /></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><g:formatDate date="${purchaseInstance.dateCreated }"/></td>
									<td>${purchaseInstance.createdBy.fname }</td>
									<td><g:formatNumber number="${purchaseInstance.totalCost}" type="number"/></td>
									<td><g:formatNumber number="${indvCost}" type="number"/></td>
								</tr>
							</tbody>
						</table>
						 
						<div class="tabbable">
							  <ul class="nav nav-tabs">
							    <li class="active"><a href="#p1" data-toggle="tab" id="firstTab"><g:message code="items" default="Items" /></a></li>
							    <li><a href="#p2" data-toggle="tab"><g:message code="beneficiaries" default="Beneficiaries" /></a></li>
							    <li><a href="#p3" data-toggle="tab"><g:message code="payable" default="Payables" /></a></li>
							  </ul>
							  <div class="tab-content">
								    <div class="tab-pane active" id="p1">
						    			<table class="table table-striped">
											<thead style="table-layout: fixed;">
												<tr class="headerRow">
													<th><g:message code="date" default="Item" /></th>
													<th><g:message code="date" default="Cost" /></th>
												</tr>
											</thead>
											<tbody>
												<g:each var="item" in="${purchaseInstance.items }">
													<tr>
														<td>${item.description }</td>
														<td><g:formatNumber number="${item.cost}" type="number"/></td>
													</tr>
												</g:each>
											</tbody>
										</table>
								    </div>
								    <div class="tab-pane" id="p2">
								    	<table class="table table-striped">
											<thead style="table-layout: fixed;">
												<tr class="headerRow">
													<th><g:message code="name" default="Name" /></th>
													<th><g:message code="contribution" default="Contribution" /></th>
												</tr>
											</thead>
											<tbody>
												<g:each var="b" in="${purchaseInstance.beneficiaries }">
													<tr>
														<td>${b.user.fname }</td>
														<td><g:formatNumber number="${b.amountContributed}" type="number"/></td>
													</tr>
												</g:each>
											</tbody>
										</table>
								    </div>
								    <div class="tab-pane" id="p3">
								    	<table class="table table-striped">
											<thead style="table-layout: fixed;">
												<tr class="headerRow">
													<th><g:message code="from" default="From" /></th>
													<th><g:message code="to" default="To" /></th>
													<th><g:message code="amount" default="Amount" /></th>
													<th><g:message code="status" default="Status" /></th>
												</tr>
											</thead>
											<tbody>
												<g:each var="p" in="${purchaseInstance.payables }">
													<tr>
														<td>${p.from.fname }</td>
														<td>${p.to.fname }</td>
														<td><g:formatNumber number="${p.amount}" type="number"/></td>
														<td>
															<g:if test="${p.transaction }">
																<g:message code="paid" default="Paid"/>
															</g:if>
															<g:else>
																<g:message code="unpaid" default="Unpaid"/>
															</g:else>
														</td>
													</tr>
												</g:each>
											</tbody>
										</table>
								    </div>
							  </div>
						</div>
					</div>
				</g:if>
				<g:else>
					<p><g:message  default="Purchase Not Found" code="purchase.not.found"/></p>
				</g:else>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-modal.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-tab.js"></g:javascript>
	<g:javascript src="bootstrap-tooltip.js"></g:javascript>
	<g:javascript src="bootstrap-popover.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-collapse.js"></g:javascript>

	<script type="text/javascript">
		$(document).ready(function() {
			$('.alert').slideDown("slow");
			$('#firstTab').click();
			
			$('ul.nav-tabs>li>a').click(function(){
				if($(this).hasClass('active')){  // avoid un-necessary looping
					$("div.tab-pane").addClass('active');
					$('div.tab-content>div.tab-pane').each(function(){
						$(this).removeClass('active');
					});
					$($(this).attr('href')).addClass('active');
				}
			});

			
		});
	</script>

</body>
</html>

