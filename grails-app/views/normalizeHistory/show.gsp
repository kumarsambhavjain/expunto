<%@ page import="com.samsoft.em.model.UserGroup"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- Le styles -->
<style>
body {
	padding-top: 60px;
	/* 60px to make the container go all the way to the bottom of the topbar */
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'expunto.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body>
	<g:include view="/nav/topBar.gsp" />
	<div class="container-fluid">
		<g:if test="${flash.message }">
			<div class="row-fluid">
				<div class="span12">
					<div class="alert alert-success">
						<a class="close" data-dismiss="alert">×</a>
						${flash.message }
					</div>
				</div>
			</div>
		</g:if>
		<div class="row-fluid">
			<div class="span2" style="padding: 8px 0;">
				<g:include view="nav/nav.gsp"  params="[menuSelected:5]"/>
			</div>
			<div class=" span8" id="mainContent">
				<legend>
					<g:message code="normalization.history" default="Normalization History" />
				</legend>
				
				<g:if test="${payable!=null && normalizedHistorylist!=null&&normalizedHistorylist.size()>0 }">
				
					
						<table class="table table-striped" style="margin-top: 30px">
							<thead style="table-layout: fixed;">
								<tr class="headerRow">
									<th><g:message code="dateCreated" default="Date Created" /></th>
									<th><g:message code="from" default="From" /></th>
									<th><g:message code="to" default="To" /></th>
									<th><g:message code="description" default="Description" /></th>
									<th><g:message code="amount" default="Amount" /></th>
									<th><g:message code="created.by" default="Created By"/></th>
									<th><g:message code="status" default="Status"/></th>
								</tr>
							</thead>
							<tbody>
								<g:if test="${payable}">
										<tr>
											<td><g:formatDate date="${payable.dateCreated }" /></td>
											<td>${payable.from.getFullName()}</td>
											<td>${payable.to.getFullName()}</td>
											<td>${payable.description}</td>
											<td><g:formatNumber number="${payable.amount }" type="number" /></td>
											<td>${payable.createdBy.getFullName()}</td>
											<td><g:message default="Unpaid" code="unpaid"/></td>
										</tr>
								</g:if>
								<g:else>
									<tr>
										<td colspan="4"><g:message code="no.results.found" default="No Results Found" /></td>
									</tr>
								</g:else>
							</tbody>
						</table>
					
					<legend>
						<g:message code="derived.from" default="Derived From"/>
					</legend>
					<%--<g:render template="../payable/genericPayableTemplate" model="[result:normalizedHistorylist]"></g:render>
					--%><table class="table table-striped" style="margin-top: 30px">
						<thead style="table-layout: fixed;">
							<tr class="headerRow">
								<th><g:message code="dateCreated" default="Date Created" /></th>
								<th><g:message code="from" default="From" /></th>
								<th><g:message code="to" default="To" /></th>
								<th><g:message code="description" default="Description" /></th>
								<th><g:message code="amount" default="Amount" /></th>
								<th><g:message code="status" default="Status"/></th>
							</tr>
						</thead>
						<tbody>
							<g:if test="${normalizedHistorylist }">
								<g:each in="${normalizedHistorylist }" var="p">
									<tr>
										<td><g:formatDate date="${p.dateCreated }" /></td>
										<td>${p.from.getFullName()}</td>
										<td>${p.to.getFullName()}</td>
										<td>${p.description}</td>
										<td><g:formatNumber number="${p.amount }" type="number" /></td>
										<td><g:if test="${p.transaction }">
												<g:if test="${p.normalizeHistory }">
													<a href="#" rel="tooltip" data-placement="top"
														data-original-title="On <g:formatDate date="${p.transaction.dateCreated}"/>">
														<g:message code="normalized.paid"
															default="Normalized & Mark Paid" />
													</a>
												</g:if>
												<g:else>
													<a href="#" rel="tooltip" data-placement="top"
														data-original-title="On <g:formatDate date="${p.transaction.dateCreated}"/>">
														<g:message code="paid.on" default="Paid" />
													</a>
												</g:else>
											</g:if> <g:else>
												<g:message code="unpaid" default="Unpaid" />
											</g:else></td>
									</tr>
								</g:each>
							</g:if>
							<g:else>
								<tr>
									<td colspan="4"><g:message code="no.results.found" default="No Results Found" /></td>
								</tr>
							</g:else>
						</tbody>
					</table>
				</g:if>
				<g:else>
					<h3><g:message code="no.details.found" default="No Details Found"/></h3>
				</g:else>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-transition.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-button.js"></g:javascript>
	<g:javascript src="bootstrap-tooltip.js"></g:javascript>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>

</body>
</html>
