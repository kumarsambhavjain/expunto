<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="google-site-verification" content="ZtFEIonMGqCNRCa_sVXCC1EWHeo0TkVSp9ZKtA1FBOU" />
<meta charset="utf-8">
<title>Expunto: Group Expense Manager</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Kumar Sambhav Jain">

<!-- Le styles -->
<style>
body {
	padding-top: 60px;
}
</style>
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap.css')}" />
<link type="text/css" rel="stylesheet"
	href="${resource(dir:'css',file:'bootstrap-responsive.css')}" />

<link rel="shortcut icon"
	href="${resource(dir:'images',file:'favicon.ico')}">
</head>

<body style="padding-top: 0px;">
<script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'white'
 };
 </script>
<%
    ReCaptcha c = ReCaptchaFactory.newReCaptcha("6Lf-ydgSAAAAAGO0fzfvupxiWQyEB06URyuSEmMP", "6Lf-ydgSAAAAANQMxE8ET-FS1VPgXuBC8_CdDwBx", false);
%>
		<div style="background-color: #09538B;height: 90px;margin-bottom: 15px;padding-top: 10px;" class="row">
			<div class="span5 offset1" style="padding-top: 22px">
				<h1 style="color: white;vertical-align:middle;font-size: 50px;">
					Expunto <span style="font-size: 15px;color: white;">Group Expense Manager</span>
				</h1> 
			</div>
			<div class="span7">
				<div class="row">
					<form  action='${postUrl}' method='POST' id='loginForm'	autocomplete="on">
					  	<div class="span2 offset2">
						  	<label style="color: white;">Email</label>
						    <input type="email" placeholder="Email address" class="input-medium" name="j_username" id="username" autofocus="autofocus" required="required" tabindex="1">
						    <label class="checkbox" style="color: white;">
						    	<input type="checkbox" name='${rememberMeParameter}'> Remember Me
						    </label>
					  	</div>
					  	<div class="span2" style="margin-left: 10px;">
					  		<label style="color: white;">Password</label>
					  		<input type="password" placeholder="Password"  class="input-medium" required="required" name="j_password" id="password" tabindex="2"/>
					  		<label>
						    	<g:link controller="register" action="resetPassword" title="Password will be sent to your email address" style="color:white;"><g:message code="reset.password" default="Reset Password"/></g:link>
						    </label>
					  	</div>
					  	<div class="span1" style="margin-top: 23px;margin-left: 10px;">
					  		<input type="submit" value="Login" class="btn">
					  	</div>
					  </form>
				</div>
			</div>
		</div>
		<div class="row" style="padding-top: 20px;">
			<div class="span8" style="padding: 5px;">
				<div id="myCarousel" class="carousel slide">
				  <!-- Carousel items -->
				  <div class="carousel-inner">
				    <div class="active item">
				    	<img alt="" src="../images/slider/home1.png" width="100%;">
				    </div>
				    <div class="item">
				    	<img alt="" src="../images/slider/monthly.png" width="100%;">
				    </div>
				  </div>
				  <!-- Carousel nav -->
				  <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				  <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
				</div>
			</div>
			<div class="span3 offset1" style="padding-top: 20px;">
					<g:form class="" method="post" action="createNewUser" controller="register" name="id_createNewUser" useToken="true">
						<h1>
							<g:message code="user.create.new.account"
								default="New Account" />
						</h1>
						<div class="row-fluid" style="margin-top: 10px;">
							<input type="text" placeholder="First Name" class="span5" required="required" name="fname" autocomplete="on"/>
							<input type="text" placeholder="Last Name" class="span4" required="required" name="lname" autocomplete="on"/>
						</div>
						<div class="row-fluid">
								<input type="email" class="span10 focused" id="username"
										name="username" required="required" 
										autocomplete="on" placeholder="Your Email Address"> 
						</div>
						<div class="row-fluid">
							<input type="password" class="span10" placeholder="Password"  required="required" id="password" name="password"/>
						</div>
						<div class="row-fluid">
							<input type="password" class="span10" placeholder="Re-type Password" required="required" id="re_password" name="re_password"/>
						</div>
						<div class="row-fluid">
							<div class="span10">
								<%= out.print(c.createRecaptchaHtml(null, null))%>
							</div>
						</div>
						<div class="row-fluid"  style="margin-top: 10px;">
							<input type="submit" class="btn btn-primary btn-large" value="Register" id="submitButton" width="100%"/> 
						</div>
				</g:form>
				<g:if test="${flash.successMessage }">
					<div class="alert alert-success">
			  			<a type="button" class="close" data-dismiss="alert">×</a>
			  			${flash.successMessage}
					</div>
				</g:if>
				<g:if test="${flash.errorMessage }">
					<div class="alert alert-error">
			  			<a class="close" data-dismiss="alert">×</a>
			  			${flash.errorMessage}
					</div>
				</g:if>
				<g:if test="${flash.message }">
						<div class="alert alert-success">
				  			<a type="button" class="close" data-dismiss="alert">×</a>
				  			${flash.message}
						</div>
				</g:if>
				<g:if test="${flash.invalidCaptcha }">
						<div class="alert alert-error">
				  			<a type="button" class="close" data-dismiss="alert">×</a>
				  			<g:message code="${flash.invalidCaptcha }" default="Invalid Captcha. Please retry."/>
						</div>
				</g:if>
				<g:if test="${flash.userInstanceWithErrors}">
					<g:eachError bean="${flash.userInstanceWithErrors}">
						<g:if test="${flash.userInstanceWithErrors.errors.hasFieldErrors("username") }">
							<div class="alert alert-error">
				  			<a class="close" data-dismiss="alert">×</a>
				  			<g:message code="username.exists" default="A user is already registered with email '${it.rejectedValue}'"/>
							</div>						
						</g:if>
						<g:else>
						<div class="alert alert-error">
				  			<a class="close" data-dismiss="alert">×</a>
				  			<g:message error="${it }"/>
						</div>
						</g:else>
					</g:eachError>
				</g:if>
			</div>
		</div>
	<!-- /container -->
	<!-- Placed at the end of the document so the pages load faster -->
	<g:javascript src="jquery.js"></g:javascript>
	<g:javascript src="bootstrap-alert.js"></g:javascript>
	<g:javascript src="bootstrap-dropdown.js"></g:javascript>
	<g:javascript src="bootstrap-carousel.js"></g:javascript>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#myCarousel').carousel({
				interval	:	3000
			});
		});
	</script>
 </body>
</html>
