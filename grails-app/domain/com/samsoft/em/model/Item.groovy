package com.samsoft.em.model

class Item {

	String description
	int cost
	Date dateCreated

	static belongsTo = [purchase : Purchase]

    static constraints = {
    	description blank:false,maxLength:100
    	cost min:1,max:500000
    }


    public String toString(){
    	"Desc: ${description} , Cost: ${cost}"
    }
}
