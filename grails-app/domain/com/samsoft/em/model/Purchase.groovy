package com.samsoft.em.model


class Purchase {

	String description
	Date dateCreated
	Date dateOfPurchase
	int totalCost;
	UserGroup group // Every purchase is made in context of a group. One user can belong to many groups.
	User createdBy
	

	List<Item> items
	List<Payable> payables
	List<Beneficiary> beneficiaries

	static hasMany = [items:Item,payables:Payable,beneficiaries: Beneficiary]
    static constraints = {
    	description nullable:false,maxLength:1000
    	dateOfPurchase nullable:false,updatable:false
    	totalCost min:1    	
    	group nullable:false
    	description widget:'textArea'
    }

    public int getTotalCost(){
    	int total  = 0;
    	items.each{
    		total+=it.cost
    	}
    	return total
    }
	
	public int getTotalContribution(){
		int total = 0;
		beneficiaries.each{
			total+=it.amountContributed
		}
		return total;
	}

}
