package com.samsoft.em.model
class Payable {
	String description
	User from
	User to
	int amount
	boolean verified = false      // future enhancement. Each payable should be first verified by 'from' user!!!
	UserGroup group   // every payable is in context of 'group of users'. This is required when normalizing payables.
	Date dateCreated
	User createdBy
	Transaction transaction
	long normalizeHistory  // If this payable is normalized, how was it concluded is captured by this   
	
	static belongsTo = [purchase:Purchase]

	static constraints = {
		description blank:false,maxLength:500
		from nullable:false,updateable:false
		to nullable:false,updateable:false
		group nullable:false,updateable:false
		amount min:1,max:900000,updateable:false
		transaction nullable:true,fetch: 'join',lazy: false             // null transaction signifies that the payable is still unpaid.
		purchase nullable:true    // payable can be added outside context of a purchase also.
		normalizeHistory nullable:true // 
	}
	

	public String toString(){
		println "From: ${from.fname} To: ${to.fname} Amount: ${amount}"
	}

	/**
	 * Use it very carefully. Unattended changes might get persited while Hibernate session is flushed.Add given amount to this payable.	 * 
	 * @param amount
	 */
	public void addPayableAmount(int amount) {
		this.amount+=amount;
	}

	/**
	 * Subtracts the given amount from this payable.
	 * @param amount
	 */
	public void subtractAmount(int amount){
		this.amount-=amount;
	}

	/**
	 * Swaps the 'to' and 'from' member and negate the amount. 
	 * This method should be avoided on already persisted object.
	 */
	public void swapMembers(){
		User temp = from;
		from = to;
		to = temp;
		amount*=-1;
	}
}
