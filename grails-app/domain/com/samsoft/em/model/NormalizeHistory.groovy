package com.samsoft.em.model

class NormalizeHistory {

	Date dateCreated
	User createdBy
	String comments
	
	static hasMany = [payables : Payable]   // set of paid payables. These all were mark paid while normalizing.

	static constraints = {
		createdBy nullable:false,updatable:false
	}
}
