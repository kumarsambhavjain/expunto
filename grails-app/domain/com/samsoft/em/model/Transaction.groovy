package com.samsoft.em.model

class Transaction {

	String comments
	Date dateCreated
	User createdBy

	static belongsTo = Payable // to maintain status of each payable, this is required. Created when a payable is marked as 'Paid'

    static constraints = {
    	comments blank:false,maxLength:600
    }
}
