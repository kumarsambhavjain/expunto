package com.samsoft.em.model

class UserExpense {
	
	Date dateCreated
	Date dateOfExpense
	String description
	int amount
	boolean generated=true
	int month
	int week
	int year
	
	static belongsTo = [user:User]

    static constraints = {
		description nullable:false,minLength:3
		amount nullable:false,min:1
		user nullable:false,index:'user_index'
		generated nullable:false
		dateOfExpense nullable:false
    }
	
	static mapping = {
		year(formula:"YEAR(DATE_OF_EXPENSE)")
		month(formula:"MONTH(DATE_OF_EXPENSE)")
		week(formula:"WEEK(DATE_OF_EXPENSE)")
	}
}
