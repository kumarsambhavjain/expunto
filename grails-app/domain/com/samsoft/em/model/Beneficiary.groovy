package com.samsoft.em.model

class Beneficiary implements Cloneable{

	Date dateCreated
	int amountContributed
	User user


    public Beneficiary(User user){
        this.user = user
    }

	static belongsTo = [purchase:Purchase]

    static constraints = {
    	user nullable:false
    	amountContributed min:0,max:500000
    }

    public String toString(){
    	"${user?.fname} has contributed ${amountContributed}"
    }
}
