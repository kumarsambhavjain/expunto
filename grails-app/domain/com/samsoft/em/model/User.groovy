package com.samsoft.em.model

import com.samsoft.em.model.sec.SecUser;
import com.samsoft.em.model.User;


class User  extends SecUser{

	String fname
	String lname
	String mobile
	Date dateCreated
	BankDetails bank;


	static hasMany = [memberships :Membership]

	static constraints = {
		fname  blank:false,maxLength:60
		lname  blank:true,maxLength:50
		mobile nullable:true,minLength:10,maxLength:11,unique:true
		bank   nullable:true
	}
	
	public String toString(){
		"${fname} ${lname}"
	}
	
	public String getFullName(){
		"${fname} ${lname}"
	}
	public int hashCode() {
		super.hashCode()
	}
	
	public boolean equals(Object o){
		if(o==null)
			return false
		if(!getClass().equals(o.getClass()))
			return false;
		User u = (User)o
		return u.username.equals(username)
			
	}
}
