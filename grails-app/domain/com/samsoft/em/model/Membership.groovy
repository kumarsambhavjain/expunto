package com.samsoft.em.model

class Membership {
	
	Date dateCreated
	User user;
	UserGroup group

    static constraints = {
		user nullable:false
		group nullable:false
		group unique : 'user'
    }
	
	
	 static Membership link(user,group){
		 def m  = Membership.findByUserAndGroup(user,group)
		 if(!m){
			 m  = new Membership()
			 user?.addToMemberships(m)
			 group?.addToMemberships(m)
			 m.save()
			 return m
		 }else{
		 	return null;
		 }
	 }
	 
	 static void unlink(user,group){
		 def m  = Membership.findByUserAndGroup(user,group)
		 if(m){
			 user?.removeFromMemberships(m)
			 group?.removeFromMemberships(m)
		 }
	 }

	 /**
	 * get ids of users for given groupID
	 */
	 static List<Long> getUsersInGroup(long groupID){
	 	Membership.createCriteria(){
	 		eq ("group.id",groupID)
	 		projections{
	 			id()
	 		}
	 	}.list()
	 }

	 static List<Long> getGroupIDsForUser(long userID){
	 	Membership.createCriteria(){
	 		eq ("user.id",userID)
	 		projections{
	 			id()
	 		}
	 	}.list()
	 }
}
