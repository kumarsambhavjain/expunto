package com.samsoft.em.model

class BankDetails {
	
	String accountNo // optional. to facilitate online transfer.
	String ifscCode
	String bankName
	String bankUrl

	static belongsTo = [user: User]

    static constraints = {
		accountNo nullable:false,maxLength:30
		ifscCode nullable:true,maxLength:5
		bankUrl nullable:true
		bankName nullable:false
    }

}
