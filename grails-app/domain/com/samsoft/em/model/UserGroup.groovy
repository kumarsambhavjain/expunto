package com.samsoft.em.model

class UserGroup {

	String groupName
	Date dateCreated
	User createdBy
	User moderator
	String groupKey

	//static hasMany = [users : User]
	static hasMany = [memberships :Membership]

    static constraints = {
    	groupName blank:false,unique:true
    	createdBy nullable:false,display:false
    	moderator nullable:false,display:false
		groupKey nullable:false,unique:true,length:10,minLength:6,matches:'[a-zA-Z0-9_]{6,12}',display:false
    }
}
