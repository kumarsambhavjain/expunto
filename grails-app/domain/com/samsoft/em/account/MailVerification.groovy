package com.samsoft.em.account

import com.samsoft.em.model.sec.SecUser;

class MailVerification {

	Date dateCreated
	Date lastUpdated
	String sentTo
	String verificationKey
	boolean verfied
	boolean mailSent

	static constraints = {
		sentTo email:true,blank:false
		verificationKey blank:false
	}
}
