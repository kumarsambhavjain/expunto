package com.samsoft.em.service
import org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware

import com.samsoft.em.account.MailVerification
import com.samsoft.em.model.*
class MailUtilityService implements  ApplicationContextAware {

	def static scope = "request"
	def springSecurityService
	def authenticatedUser
	def theUser = null;
	private ApplicationTagLib g

	void setApplicationContext(ApplicationContext applicationContext) {
		g = applicationContext.getBean(ApplicationTagLib)

		// now you have a reference to g that you can call render() on
	}
	/**
	 * mail service by the mail grails plugin.
	 */
	def mailService

	def sendPayableAlert(Payable payableInstance) {
		if(!theUser)
			theUser = User.get(springSecurityService.principal.id)
		try{
			Thread t = new Thread(new Runnable(){
						public void run(){
							mailService.sendMail {
								to   payableInstance.from.username
								from theUser.username
								subject "Expunto INFO: Payable Added"
								html  """
									<p>
									You owe ${payableInstance.to.fname} an amount of Rs. ${payableInstance.amount} for '${payableInstance.description}'.\n
									See details <a href='http://www.expunto.com/payable/show/${payableInstance.id}'>here</a>
									</p>
								  """
							}
						}
					});
			t.start();
		}catch(Exception ex){
			log.error("Error while sending payable alert mail",ex)
		}
		return true
	}


	def sendPurchaseAlert(Purchase purchase){
		if(!theUser)
			theUser = User.get(springSecurityService.principal.id)
		try{
			purchase?.payables?.each{ payableInstance ->
				Thread t = new Thread(new Runnable(){
							public void run(){
								mailService.sendMail {
									to   payableInstance.from.username
									from theUser.username
									subject "Expunto INFO: Purchase Added"
									html  """
									<p>
									You owe ${payableInstance.to.fname} an amount of Rs. ${payableInstance.amount} for '${payableInstance.description}'.\n
									See details <a href='http://www.expunto.com/purchase/show/${purchase.id}'>here</a>
									</p>
								  """
								}
							}
						});
				t.start();
			}
		}catch(Exception ex){
			log.error('Error while sending purchase alert mail.',ex)
		}
		return true
	}

	def sendAccountVerficationMail(MailVerification verfication){
		mailService.sendMail{
			to verfication.sentTo
			subject "Expunto: Account Verification"
			html """    <br>
						Thanks for registering with Expunto.<br>
						Please activate your account by clicking <a href=http://www.expunto.com/register/verifyAccount?w960=${verfication.verificationKey}&t810=${verfication.sentTo}>here</a>
						<br>
						Kindly ignore if you were not expecting this mail.
					 """
		}
		verfication.mailSent = true
	}
	
	def sendNewPassword(String email,String password){
		mailService.sendMail{
			to email
			subject "Expunto: Password Reset"
			html """    <br>
						Hi, <br>

						Your new Expunto password is : ${password}. <br>
						This is auto-generated password. Kindly reset your password.<br>
						Please ignore if you were not expecting this mail.
					 """
		}
	}
	/**
	 * Mail will be send in seperate thread.
	 * @param payable
	 * @return
	 */
	def sendPayableReminder(Payable payable){
		Thread t = new Thread(new Runnable(){
			public void run(){
				mailService.sendMail{
					to payable.from.username
					subject "Expunto: Payable Reminder"
					html """    <br>
						Hi ${payable.from.fname}, <br>

						You owe Rs ${payable.amount} to ${payable.to.fname} for '${payable.description}'.<br>
						See details <a href='http://www.expunto.com/payable/show/${payable.id}'>here</a><br><br>
						Regards, <br>
						Expunto  Team
						
				 """
				}
			}
		});
		t.start();
	}
}
