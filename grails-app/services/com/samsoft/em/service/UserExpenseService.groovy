package com.samsoft.em.service


import org.hibernate.Hibernate;
import org.hibernate.type.DateType

import com.samsoft.em.model.UserExpense

class UserExpenseService {
	
	def springSecurityService
	
	/**
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param frequency 0 for week, 1 for month, 2 for year
	 * @return
	 */
    def getUserExpenses(Date fromDate,Date toDate,int frequency) {		
		if(fromDate==null||toDate==null||fromDate<toDate){
			def c = UserExpense.createCriteria()
			def results = c {
				between("dateOfExpense", fromDate, toDate)
				eq("user.id", springSecurityService.principal.id)
				projections {
					switch(frequency){
						case 0: //weekly
							groupProperty('year')
							groupProperty('month')
							groupProperty('week')
							order('year','asc')
							order('month','asc')
							order('week','asc')
						break;
						case 1://monthly
							groupProperty('year')
							groupProperty('month')
							order('year','asc')
							order('month','asc')
						break;
						case 2: //yearly
							groupProperty('year')
							order('year','asc')
						break;
					}
					sum('amount')
				}
			}
			return results
		}else{
			return null
		}
    }
}
