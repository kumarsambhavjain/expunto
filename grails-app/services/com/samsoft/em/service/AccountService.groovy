package com.samsoft.em.service

import grails.plugins.springsecurity.SpringSecurityService

import com.samsoft.em.account.MailVerification
import com.samsoft.em.model.User
import com.samsoft.em.model.sec.SecUser

class AccountService {

	static transactional = true
	static scope = "request"
	MailUtilityService mailUtilityService
	SpringSecurityService springSecurityService

	private String getRandomString(int length){
		StringBuffer sb = new StringBuffer(length);
		int r =0;
		for(int i=0;i<length;i++){
			r = (int)(Math.random()*30307900)
			if(r%2==0){
				sb.append(r%10)
			}
			else if(r%3==0){
				sb.append((char)((r%26)+97))
			}
			else{
				sb.append((char)((r%26)+65))
			}
		}
		return sb.toString()
	}

	def createNewAccount(User user) {
		// save user instance
		user=user.save(failOnError:true)
		// send verification mail
		MailVerification verification = new MailVerification()
		verification.sentTo =  user.username;
		verification.verificationKey = getRandomString(40)
		mailUtilityService.sendAccountVerficationMail(verification)
		if(verification.mailSent){
			verification.save(failOnError:true);
		}
	}

	def verifyAccount(String key,String email){
		def verfCriteria = MailVerification.createCriteria()
		MailVerification result = verfCriteria.get {
			eq  "verificationKey",key
			eq	"sentTo",email
		}
		if(result){
			result.verfied = true
			SecUser account = SecUser.findByUsername(email)
			account.enabled = true
			account.accountLocked = false
			account.save(failOnError:true,flush:true)
			result.save(failOnError:true)
			return true
		}else{
			return false;
		}
	}

	def resetPassword(String email){
		def user = SecUser.findByUsername(email)
		if(user){
			String newPassword = getRandomString(12);
			user.setPassword(newPassword)
			user.save(failOnError:true)
			mailUtilityService.sendNewPassword(email,newPassword)
		}else{
			return null
		}
	}

	def deleteAccount(){
		// TODO
	}

	def checkExistingEmail(String email){
		SecUser.withCriteria { eq("username", email) }.unique()==null
	}

	def checkExistingMobile(String mobile){
		// TODO
	}

}
