package com.samsoft.em.service

import com.samsoft.em.model.Membership;
import com.samsoft.em.model.User;
import com.samsoft.em.model.UserGroup

class UserService {

	static scope = "prototype"
	static transactional = true
	def springSecurityService
	
   /**
    * Checks if the currently logged in user belong to given group id
    * @param groupId
    * @return
    */
   def belongsToGroup(long groupId){
	   def result  = Membership.executeQuery("select m.id from Membership m where m.user.id=? and m.group.id=?",[springSecurityService.principal.id,groupId])
	   return result != null
   }
   
   /**
    * 
    * @param currentPassword
    * @param newPassword
    * @return  true if password was changed successfully.
    */
   def changePassowrd(String currentPassword,String newPassword){
	   if(currentPassword && newPassword){
		   def user = springSecurityService.currentUser
		   if(springSecurityService.encodePassword(currentPassword).equals(user.password)){
			   user.setPassword(newPassword);
			   user.save(failOnError:true)
		   }else{
		   		// user did not supply correct current password
		   		return false;
		   }
	   }else{
	   	return false
	   }
   }
	
}
 