package com.samsoft.em.service
import java.util.List;
import  com.samsoft.em.model.*

class PurchaseService {

	static scope = "session"
	def springSecurityService
	def theUser = null;
	private static Comparator<Beneficiary> asc = new Comparator<Beneficiary>(){
		public int compare(Beneficiary b1,Beneficiary b2){
			return b1.amountContributed - b2.amountContributed
		}
	};

	private static Comparator<Beneficiary> dsc = new Comparator<Beneficiary>(){
		public int compare(Beneficiary b1,Beneficiary b2){
			return - b1.amountContributed + b2.amountContributed
		}
	};

	/**
	 * This is good one. Takes instance of purchase that has items and beneficiaries. Computes the resultant payables and set them in the instance and also returns the same.
	 * @param purchase   instance of Purchase
	 * @return   List of Payable  null in case of error
	 */
	def evaluatePayables(Purchase purchase) {
		if(purchase!=null&&purchase.items!=null&&purchase.items.size>0&&purchase.beneficiaries!=null&&purchase.beneficiaries.size()>1){
			int totalCost = purchase.getTotalCost()
			int totalContr = purchase.getTotalContribution();
			if(totalCost==totalContr){
				List<Beneficiary> beneficiaries =  new ArrayList<Beneficiary>(purchase.beneficiaries.size())
				purchase.beneficiaries.each{
					beneficiaries.add(it.clone())
				}
				int bcount = beneficiaries.size();
				int individualCost = totalCost/bcount
				PriorityQueue<Beneficiary> givers = new PriorityQueue<Beneficiary>(1,asc);
				PriorityQueue<Beneficiary> takers = new PriorityQueue<Beneficiary>(1,dsc);

				// fill the PQs
				for(Beneficiary b: beneficiaries){
					if(b.amountContributed<individualCost){
						givers.offer(b);
					}else{
						takers.offer(b);
					}
				}
				int amountPayable = -1;
				int amountRecievable = -1;
				Beneficiary giver = null,taker = null;
				List<Payable> payables = new ArrayList<Payable>();
				while(givers?.size()!=0&&takers?.size()!=0){
					giver = givers.peek();
					taker = takers.peek();
					amountPayable = individualCost - giver.amountContributed
					amountRecievable  = taker.amountContributed - individualCost
					if(amountPayable<1 ||amountRecievable<1){   // no payables required in this case
						if(amountPayable<1){
							givers.poll();
						}
						if(amountRecievable<1){
							takers.poll();
						}
						continue;
					}
					if(amountRecievable==amountPayable){
						giver = givers.poll();
						taker = takers.poll();
						payables.add(new Payable(from:giver.user,to:taker.user,amount:amountRecievable))
					}
					else if(amountRecievable>amountPayable){
						givers.poll();
						taker= takers.poll();
						taker.amountContributed  =  taker.amountContributed - amountPayable
						takers.offer(taker)
						payables.add(new Payable(from:giver.user,to:taker.user,amount:amountPayable))
					}else if (amountRecievable<amountPayable){
						takers.poll()
						giver = givers.poll()
						giver.amountContributed =giver.amountContributed + amountRecievable
						givers.offer(giver)
						payables.add(new Payable(from:giver.user,to:taker.user,amount:amountRecievable))
					}

				}
				log.info("calculated")
				purchase.payables = payables
				return payables
			}else{
				log.warn(purchase,"contribution and cost do not match")
				return null;
			}
		}else{
			log.info('Invalid purcahse');
			return null;
		}
	}

	/**
	 * This will also check that the purchase belongs to one of enrolled group.
	 * @param purchaseId
	 * @return
	 */
	def getPurchaseDetailsByID(long purchaseId){
		if(!theUser) // get user if its not already there
			theUser = User.read(springSecurityService.principal.id)
		def criteria = Purchase.createCriteria()
		def result = criteria.get{
			eq "id",purchaseId
			'in' "group",theUser.memberships.group
			join 'items'
			join 'payables'
			join 'beneficiaries'
		}
		return result;
	}
}
