package com.samsoft.em.service

import com.samsoft.em.model.Membership;
import com.samsoft.em.model.User;
import com.samsoft.em.model.UserGroup;

class UserGroupService {
	
	static scope = "session"
	static transactional = true
	
	def springSecurityService
	def theUser = null;

    def createNewGroup(UserGroup group) {

    }
	
	/**
	 * Get only member id and fname.
	 * @param groupId
	 * @return
	 */
	def getGroupMembers (def groupId){
		def members = Membership.executeQuery("select distinct m.user.id,m.user.fname,m.user.lname from Membership m where m.group.id = ?",[groupId]);
		return members;
	}
	
	/**
	 * Get associated group of given user
	 * @param userId user id
	 * @return   Groups to which the user belongs
	 */
	def getUsersGroups(def userId){
		if(userId){
			return Membership.executeQuery("select  m.group from Membership m where m.user.id=?",[userId])
		}else{
			return null;
		}
	}
	
	
	def getUserInGroups(Set<Long> groupIDs){
		if(groupIDs){
			def criteria = Membership.createCriteria();
			def result = criteria.list {
							'in' "group.id",groupIDs
							projections {
								distinct('user')
							}
						  }
			// TODO fix this using projections.
			def r = []
			result.each {
				r.add([uid:it.id,name:it.toString()])
			}
			return r
		}else{
			return null;
		}
	}
}
