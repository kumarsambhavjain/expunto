package com.samsoft.em.service



import java.util.Comparator;

import org.apache.commons.collections.map.HashedMap
import org.hibernate.FetchMode

import com.samsoft.em.model.*



class PayableService {


	private static class UserAmount{
		public User user
		public int amount
	}

	private static Comparator<UserAmount> dsc = new Comparator<UserAmount>(){
		public int compare(UserAmount b1,UserAmount b2){
			return - b1.amount + b2.amount
		}
	};

	static scope = "session"
	def springSecurityService
	def theUser = null;
	def authenticatedUser

	/**
	 * Return list of payable the current user needs to pay.This is independent of group.
	 * @return null if no such payables.
	 */
	def getUserPayableTos(){
		return Payable.executeQuery("select p from Payable p where p.from.id = ? and p.transaction is null order by p.id desc",[
			springSecurityService.principal.id
		])
	}


	def getUserPayableTos(String sortBy,String ascOrDesc){
		def c = Payable.createCriteria()
		def result = c	{
			eq  "from.id",springSecurityService.principal.id
			isNull "transaction"
			order sortBy,ascOrDesc
		}
	}
	/**
	 * Returns list of payable the other users owe to this member. This is independent of group.
	 * @return
	 */
	def getUserRecievable(){
		return Payable.executeQuery("select p from Payable p where p.to.id = ? and p.transaction is null order by p.id desc",[
			springSecurityService.principal.id
		])
	}

	/**
	 * 
	 * @param sort sort by which property
	 * @param order ascending or descending
	 * @return
	 */
	def getUserRecievable(String sortBy,String ascOrDesc){
		def c = Payable.createCriteria()
		def result = c	{
			eq  "to.id",springSecurityService.principal.id
			isNull "transaction"
			order sortBy,ascOrDesc
		}
	}


	/**
	 * Gets unpaid payable of the group, Form a directed weighted graph out of it and minimize the number of
	 *  payables keeping the valency of each node unchanged. Love this method.
	 * @param groupId Takes id of a group.
	 * @return List of payable that will have same effect when all the unpaid payable of this group is combined.
	 */
	def getNormalizedPayables(long groupId){
		def summedPayables = Payable.executeQuery("select  p.from,sum(p.amount)  from Payable p where p.group.id = ? and p.transaction is null group by p.from order by p.from",[groupId]);
/*		if(summedPayables==null||summedPayables.size()==0)
			return null*/
		def summedReceivables = Payable.executeQuery("select  p.to,sum(p.amount)  from Payable p where p.group.id = ? and p.transaction is null group by p.to order by p.to",[groupId]);
		/*if(summedReceivables==null||summedReceivables.size()==0)
			return null*/
		HashedMap<User,Integer> recievableMap1 = new HashedMap<User,Integer>();
		summedReceivables.each{
			recievableMap1.put(it[0], it[1]) // user , Integer
		}
		def amount = null;
		summedPayables.each{
			amount = recievableMap1.get( it[0], null)
			if(amount){
				recievableMap1.put( it[0], amount - it[1])
			}else{
				// amount not found
				recievableMap1.put(it[0],it[1]*-1)
			}
		}

		PriorityQueue<UserAmount> givers = new PriorityQueue<UserAmount>(5,dsc);
		PriorityQueue<UserAmount> takers = new PriorityQueue<UserAmount>(5,dsc);

		Integer tempAmount = null;
		for(User u :recievableMap1.keySet()){
			tempAmount = recievableMap1.get(u);
			if(tempAmount>0){
				takers.offer(new UserAmount(user:u,amount:tempAmount))
			}else{
				// negative recievables.
				givers.offer(new UserAmount(user:u,amount:-1*tempAmount))
			}
		}
		ArrayList<Payable> result = new ArrayList<Payable>();
		UserAmount taker =null,giver=null;
		UserAmount tempUserAmount = null;
		Integer transferAmount = null;
		while(takers?.size>0&&givers?.size>0){
			taker = takers.peek()
			giver = givers.peek()

			if(taker.amount == giver.amount){
				result.add(new Payable(from:giver.user,to:taker.user,amount:taker.amount))
				takers.poll()
				givers.poll()
			}else if (taker.amount > giver.amount){
				result.add(new Payable(from:giver.user,to:taker.user,amount:giver.amount))
				giver=givers.poll()
				tempUserAmount = takers.poll()
				tempUserAmount.amount-=giver.amount
				takers.offer(tempUserAmount)
			}else{
				result.add(new Payable(from:giver.user,to:taker.user,amount:taker.amount))
				taker =takers.poll()
				tempUserAmount = givers.poll()
				tempUserAmount.amount-=taker.amount
				givers.offer(tempUserAmount)
			}
		}
		/*if(result.size()==summedPayables.size()||result.size()==summedReceivables.size())
			return null*/
		return result;
	}

	/**
	 * This method will be used to mark 'multiple' unpaid payable as paid.
	 * @param payableIDs List of unpaid payables ids
	 * @param message  comment to added in transaction
	 * @return payables. null in case of error.
	 */
	def markMultiplePaid(payableIDs,message){
		def payables = Payable.executeQuery("select p from Payable p where p.id in (:payableIDs) and p.transaction=null",[payableIDs:payableIDs])
		if(payables){
			if(!theUser) // get user if its not already there
				theUser = User.read(springSecurityService.principal.id)
			payables.each{
				it.transaction = new Transaction(comments:message,createdBy:theUser)  // setting a transaction in payable makes it paid!!
				it.save(failOnError:true,flush:true)
			}
			return payables
		}else{
			return null
		}
	}

	/**
	 * This method will get the normalized payables, mark the all unpaid payable as settled and save the normalized payables.
	 * This functionality will trim the information about unpaid payables in a group.
	 * @param groupId  id of group
	 * @param message Message to be added in transaction.
	 * @return normalizedPayables if successfull or null
	 */
	def saveNormalizedPayableForGroup(long groupId,String message){
		if(!theUser) // get user if its not already there
			theUser = User.read(springSecurityService.principal.id)
		def unpaidPayables = Payable.executeQuery("select p from Payable p where p.group.id=? and p.transaction=null",[groupId])
		if(unpaidPayables){
			def normalizedPayables = getNormalizedPayables(groupId)
			NormalizeHistory nh =  new NormalizeHistory(comments:message,createdBy:theUser).save(flush:true,failOnError:true);
			if(!theUser) // get user if its not already there
				theUser = User.read(springSecurityService.principal.id)
			//TODO:  Make sure that these two operations are in context of one single transaction. Find GORM transaction related handling
			unpaidPayables.each{
				it.transaction = new Transaction(comments:message,createdBy:theUser)  // setting a transaction in payable makes it paid!!
				// add the paid one to normalized history.
				nh.addToPayables(it);
				it.save(failOnError:true,flush:true)
			}
			def tgroup = UserGroup.get(groupId)
			
			normalizedPayables.each{
				it.group = tgroup
				it.description = message
				it.createdBy = theUser
				it.normalizeHistory = nh.id
				it.save(failOnError:true,flush:true)
			}
			return normalizedPayables
		}else{
			// No unpaid payables exists for this group.
			log.warn("No Unpiad Payables for group id "+groupId)
			return null;
		}
	}

	/**
	 * Returns all payables which were considered while normalizing.
	 * @param normalizationID   NormalizeHistory id 
	 * @return List of payable that were involved in the same normalization process.
	 */
	def getCoNormalizedPayableFor(long normalizationID,long payableID){
		def criteria = NormalizeHistory.createCriteria()
		
		def result = criteria.get{
			eq "id",normalizationID
			fetchMode "payables",FetchMode.JOIN
		}
		return result
	}

	/**
	* A simple method for searchhing payables. It accepts a map of param-value. 
	* It mainly aims to search by group,from,to
	* For result size,offset,sort-by,sort order, offset it will assume suitable defaults if not supplie as params.
	* will return null in case of exception or no results found.
	* List of params:-
	* 
	* offset    number default 0
	* 
	* size      number  default 50
	* orderBy   string default id
	* order     asc,dsc  default dsc
	* group     list of group(s) 
	* fromID    id of from   long
	* toID      id of to      long
	* fromDate  min date of the payable Date
	* toDate    max date of payable default to current date.
	*/
	def searchPayables(Map searchParams){
		
	}
	
	Payable getPayableByID(long payableID){
		def criteria = Payable.createCriteria();
		if(!theUser) // get user if its not already there
			theUser = User.read(springSecurityService.principal.id)
		def result = criteria.get{
			eq	"id",payableID
			'in' ("group", theUser.memberships.group)
			join 'from'
			join 'to'
		}
		return result;
	}
}

